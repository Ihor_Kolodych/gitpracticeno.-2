// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'weather_detail_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;
WeatherDetailDto _$WeatherDetailDtoFromJson(Map<String, dynamic> json) {
  return _WeatherDetailDto.fromJson(json);
}

/// @nodoc
class _$WeatherDetailDtoTearOff {
  const _$WeatherDetailDtoTearOff();

// ignore: unused_element
  _WeatherDetailDto call(
      {@JsonKey(name: 'dt') num dt,
      @JsonKey(name: 'main') WeatherMainDto main}) {
    return _WeatherDetailDto(
      dt: dt,
      main: main,
    );
  }

// ignore: unused_element
  WeatherDetailDto fromJson(Map<String, Object> json) {
    return WeatherDetailDto.fromJson(json);
  }
}

/// @nodoc
// ignore: unused_element
const $WeatherDetailDto = _$WeatherDetailDtoTearOff();

/// @nodoc
mixin _$WeatherDetailDto {
  @JsonKey(name: 'dt')
  num get dt;
  @JsonKey(name: 'main')
  WeatherMainDto get main;

  Map<String, dynamic> toJson();
  @JsonKey(ignore: true)
  $WeatherDetailDtoCopyWith<WeatherDetailDto> get copyWith;
}

/// @nodoc
abstract class $WeatherDetailDtoCopyWith<$Res> {
  factory $WeatherDetailDtoCopyWith(
          WeatherDetailDto value, $Res Function(WeatherDetailDto) then) =
      _$WeatherDetailDtoCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: 'dt') num dt,
      @JsonKey(name: 'main') WeatherMainDto main});

  $WeatherMainDtoCopyWith<$Res> get main;
}

/// @nodoc
class _$WeatherDetailDtoCopyWithImpl<$Res>
    implements $WeatherDetailDtoCopyWith<$Res> {
  _$WeatherDetailDtoCopyWithImpl(this._value, this._then);

  final WeatherDetailDto _value;
  // ignore: unused_field
  final $Res Function(WeatherDetailDto) _then;

  @override
  $Res call({
    Object dt = freezed,
    Object main = freezed,
  }) {
    return _then(_value.copyWith(
      dt: dt == freezed ? _value.dt : dt as num,
      main: main == freezed ? _value.main : main as WeatherMainDto,
    ));
  }

  @override
  $WeatherMainDtoCopyWith<$Res> get main {
    if (_value.main == null) {
      return null;
    }
    return $WeatherMainDtoCopyWith<$Res>(_value.main, (value) {
      return _then(_value.copyWith(main: value));
    });
  }
}

/// @nodoc
abstract class _$WeatherDetailDtoCopyWith<$Res>
    implements $WeatherDetailDtoCopyWith<$Res> {
  factory _$WeatherDetailDtoCopyWith(
          _WeatherDetailDto value, $Res Function(_WeatherDetailDto) then) =
      __$WeatherDetailDtoCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: 'dt') num dt,
      @JsonKey(name: 'main') WeatherMainDto main});

  @override
  $WeatherMainDtoCopyWith<$Res> get main;
}

/// @nodoc
class __$WeatherDetailDtoCopyWithImpl<$Res>
    extends _$WeatherDetailDtoCopyWithImpl<$Res>
    implements _$WeatherDetailDtoCopyWith<$Res> {
  __$WeatherDetailDtoCopyWithImpl(
      _WeatherDetailDto _value, $Res Function(_WeatherDetailDto) _then)
      : super(_value, (v) => _then(v as _WeatherDetailDto));

  @override
  _WeatherDetailDto get _value => super._value as _WeatherDetailDto;

  @override
  $Res call({
    Object dt = freezed,
    Object main = freezed,
  }) {
    return _then(_WeatherDetailDto(
      dt: dt == freezed ? _value.dt : dt as num,
      main: main == freezed ? _value.main : main as WeatherMainDto,
    ));
  }
}

@JsonSerializable(
    fieldRename: FieldRename.snake, checked: true, explicitToJson: true)

/// @nodoc
class _$_WeatherDetailDto implements _WeatherDetailDto {
  const _$_WeatherDetailDto(
      {@JsonKey(name: 'dt') this.dt, @JsonKey(name: 'main') this.main});

  factory _$_WeatherDetailDto.fromJson(Map<String, dynamic> json) =>
      _$_$_WeatherDetailDtoFromJson(json);

  @override
  @JsonKey(name: 'dt')
  final num dt;
  @override
  @JsonKey(name: 'main')
  final WeatherMainDto main;

  @override
  String toString() {
    return 'WeatherDetailDto(dt: $dt, main: $main)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _WeatherDetailDto &&
            (identical(other.dt, dt) ||
                const DeepCollectionEquality().equals(other.dt, dt)) &&
            (identical(other.main, main) ||
                const DeepCollectionEquality().equals(other.main, main)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(dt) ^
      const DeepCollectionEquality().hash(main);

  @JsonKey(ignore: true)
  @override
  _$WeatherDetailDtoCopyWith<_WeatherDetailDto> get copyWith =>
      __$WeatherDetailDtoCopyWithImpl<_WeatherDetailDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_WeatherDetailDtoToJson(this);
  }
}

abstract class _WeatherDetailDto implements WeatherDetailDto {
  const factory _WeatherDetailDto(
      {@JsonKey(name: 'dt') num dt,
      @JsonKey(name: 'main') WeatherMainDto main}) = _$_WeatherDetailDto;

  factory _WeatherDetailDto.fromJson(Map<String, dynamic> json) =
      _$_WeatherDetailDto.fromJson;

  @override
  @JsonKey(name: 'dt')
  num get dt;
  @override
  @JsonKey(name: 'main')
  WeatherMainDto get main;
  @override
  @JsonKey(ignore: true)
  _$WeatherDetailDtoCopyWith<_WeatherDetailDto> get copyWith;
}
