import 'package:custom_slider_and_painter/custom_slider.dart';
import 'package:custom_slider_and_painter/my_painter.dart';
import 'package:flutter/material.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: MyHomePage(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: CustomSlider(
          min: 20,
          max: 60,
          initialValue: 40,
          widthSlider: MediaQuery.of(context).size.width * 0.75,
        ),
      ),
    );
  }
}
