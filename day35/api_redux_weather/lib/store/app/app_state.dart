import 'package:api_redux_weather/store/pages/photos_state/photos_epics.dart';
import 'package:api_redux_weather/store/pages/photos_state/photos_state.dart';
import 'package:flutter/foundation.dart';

import 'package:redux_epics/redux_epics.dart';

class AppState {
  final WeatherState weatherState;


  AppState({
    @required this.weatherState,
  });

  factory AppState.initial() {
    return AppState(
      weatherState: WeatherState.initial(),
    );
  }

  static AppState getAppReducer(AppState state, dynamic action) {
    const String TAG = '[appReducer]';
    print('$TAG => <appReducer> => action: ${action.runtimeType}');
    return AppState(
      weatherState: state.weatherState.reducer(action),
    );
  }

  static final getAppEpic = combineEpics<AppState>([
    WeatherEpics.indexEpic,
  ]);
}