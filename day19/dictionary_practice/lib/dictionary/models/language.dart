import 'package:dictionary_practice/dictionary/dictionary_classes/genaral_language.dart';
import 'package:dictionary_practice/dictionary/dictionary_classes/setting_page_dictionary.dart';
import 'package:flutter/cupertino.dart';

class Language {
  final GeneralLanguage generalLanguage;
  final SettingsPageDictionary settingsPageDictionary;

  const Language({
    @required this.generalLanguage,
    @required this.settingsPageDictionary,
  });
}
