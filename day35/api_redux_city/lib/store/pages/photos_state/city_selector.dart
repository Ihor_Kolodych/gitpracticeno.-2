import 'package:api_redux_city/dto/city_dto.dart';
import 'package:api_redux_city/store/app/app_state.dart';
import 'package:api_redux_city/store/pages/photos_state/city_action.dart';
import 'package:redux/redux.dart';
class CitysSelector {


  static void Function(String id) selectImage(Store <AppState> store) {
    return (String id) => store.dispatch(SelectCitysAction(id: id));
  }

  static void Function() getCitysFunction(Store<AppState> store) {
    return () => store.dispatch(GetCityAction());
  }


  static String getSelectedImageId(Store<AppState> store) {
    return store.state.weatherState.selectedId;
  }

  static CityDto getSelectedImage(Store<AppState> store) {
    return store.state.weatherState.selectedCity;
  }

  static List<CityDto> getCurrentCity(Store<AppState> store) {
    return store.state.weatherState.citys;
  }
}