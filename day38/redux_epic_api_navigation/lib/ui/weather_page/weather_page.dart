import 'package:flutter/material.dart';
import 'package:redux_epic_api_navigation/ui/weather_page/weather_list_widget.dart';

class WeatherPage extends StatefulWidget {
  @override
  _WeatherPageState createState() => _WeatherPageState();
}

class _WeatherPageState extends State<WeatherPage> {

  @override
  Widget build(BuildContext context) {
        return WeatherListWidget();
  }
}
