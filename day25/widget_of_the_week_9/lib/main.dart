import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final myController = TextEditingController();
  double opacityLevel = 1.0;
  bool selected = false;
  double padValue = 0.0;
  int acceptedData = 0;

  void _changeOpacity(double value) {
    setState(() {
      opacityLevel = opacityLevel == 0 ? 1.0 : 0.0;
    });
    selected = !selected;
  }

  void _updatePadding(double value) {
    setState(() {
      padValue = value;
    });
  }

  @override
  void dispose() {
    myController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: PageView(
        children: [
          Center(
            child: GestureDetector(
              onTap: () {
                _changeOpacity(1);
                _updatePadding(padValue == 5.0 ? 50.0 : 5.0);
              },
              child: Stack(
                children: [
                  Positioned(
                    top: MediaQuery.of(context).size.height * 0.35,
                    right: MediaQuery.of(context).size.height * 0.22,
                    left: MediaQuery.of(context).size.height * 0.22,
                    child: AnimatedOpacity(
                      duration: Duration(seconds: 1),
                      opacity: opacityLevel,
                      child: AnimatedPadding(
                        padding: EdgeInsets.all(padValue),
                        duration: const Duration(seconds: 1),
                        child: AnimatedContainer(
                          duration: Duration(milliseconds: 500),
                          width: selected ? 200.0 : 100.0,
                          height: selected ? 200.0 : 100.0,
                          color: selected ? Colors.yellow : Colors.blue,
                          child: selected
                              ? Center(
                                  child: AnimatedOpacity(
                                    duration: Duration(seconds: 1),
                                    opacity: 1,
                                    child: Text('Goodbye!'),
                                  ),
                                )
                              : Center(
                                  child: AnimatedOpacity(
                                    duration: Duration(seconds: 1),
                                    opacity: opacityLevel,
                                    child: Text('Hello'),
                                  ),
                                ),
                        ),
                      ),
                    ),
                  ),
                  AnimatedPositioned(
                    width: selected ? 360.0 : 100.0,
                    height: selected ? 50.0 : 70.0,
                    top: selected ? 0.0 : 100.0,
                    duration: const Duration(seconds: 1),
                    child: Container(
                      color: Colors.blue,
                      child: const Center(child: Text('Tap me')),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Draggable<int>(
                data: 0,
                child: Container(
                  height: 100.0,
                  width: 100.0,
                  color: Colors.lightGreenAccent,
                  child: Center(
                    child: Align(
                      alignment: Alignment.center,
                      child: TextField(
                        controller: myController,
                      ),
                    ),
                  ),
                ),
                feedback: Container(
                  color: Colors.blue,
                  height: 100,
                  width: 100,
                  child: const Icon(Icons.data_usage_outlined),
                ),
                childWhenDragging: Container(
                  height: 100.0,
                  width: 100.0,
                  color: Colors.yellow,
                  child: const Center(
                    child: Text('Child When Dragging'),
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  DragTarget<int>(
                    builder: (
                      BuildContext context,
                      List<dynamic> accepted,
                      List<dynamic> rejected,
                    ) {
                      return Container(
                        height: 100.0,
                        width: 100.0,
                        color: Colors.cyan,
                        child: Center(
                          child: Text('Plus: $acceptedData'),
                        ),
                      );
                    },
                    onAccept: (int data) {
                      setState(() {
                        acceptedData += int.tryParse(myController.text);
                      });
                    },
                  ),
                  DragTarget<int>(
                    builder: (
                        BuildContext context,
                        List<dynamic> accepted,
                        List<dynamic> rejected,
                        ) {
                      return Container(
                        height: 100.0,
                        width: 100.0,
                        color: Colors.cyan,
                        child: Center(
                          child: Text('Minus: $acceptedData'),
                        ),
                      );
                    },
                    onAccept: (int data) {
                      setState(() {
                        acceptedData -= int.tryParse(myController.text);
                      });
                    },
                  ),
                ],
              ),

            ],
          ),
        ],
      ),
    );
  }
}
