import 'package:redux_epic_api_navigation/dto/photo_dto.dart';
import 'package:redux_epic_api_navigation/services/photo_service.dart';

class PhotoRepository {
  PhotoRepository._privateConstructor();

  static final PhotoRepository instance =
  PhotoRepository._privateConstructor();

  Future<List<PhotoDto>> getPhotos() async {
    var responseData = await PhotoService.instance.getPhoto();
    List<PhotoDto> citys = [];
    for (var item in responseData.response) {
      citys.add(PhotoDto.fromJson(item));
    }
    return citys;
  }
}
