import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:untitled14/providers/provider_text.dart';

class Dialog {
  static void show() {}
  Widget widget;
}

class DefaultErrorDialog implements Dialog {
  @override
  Widget widget;

  @override
  static void show(BuildContext ctx) {
    TextProvider textProvider = Provider.of<TextProvider>(ctx, listen: false);
    showDialog<String>(
      context: ctx,
      builder: (BuildContext context) => AlertDialog(
        title: const Text('AlertDialog Tilte'),
        content: Text('Are you sure you want to delete the ${textProvider.Sum} item?'),
        actions: <Widget>[
          TextButton(
            onPressed: () => Navigator.pop(context, 'Cancel'),
            child: const Text('Cancel'),
          ),
          TextButton(
            onPressed: () => Navigator.pop(
              context,
              textProvider.changeDecrimentList(),
            ),
            child: const Text('OK'),
          ),
        ],
      ),
    );
  }
}
