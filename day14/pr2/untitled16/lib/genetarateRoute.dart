import 'package:flutter/material.dart';
import 'package:untitled16/pages/fourth_page.dart';
import 'package:untitled16/pages/third_page.dart';
import './pages/firs_page.dart';
import './pages/second_page.dart';

import 'main.dart';

class RouteGenerate{
  static const ROUTE_FIRST = "/first";
  static const ROUTE_SECOND = "/second";
  static const ROUTE_MAIN= "/main";
  static const ROUTE_THIRD= "/third";
  static const ROUTE_FOURTH= "/fourth";
  //static const  onTap = '';

  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case ROUTE_FIRST:
        final page = FirstPage(settings.arguments);
        return MaterialPageRoute(builder: (context) => page);
      case ROUTE_SECOND:
        final page = SecondPage(settings.arguments);
        return MaterialPageRoute(builder: (context) => page);
      case ROUTE_THIRD:
        final page = ThirdPage(settings.arguments);
        return MaterialPageRoute(builder: (context) => page);
      case ROUTE_FOURTH:
        final page = FourthPage(settings.arguments);
        return MaterialPageRoute(builder: (context) => page);
      case ROUTE_MAIN:
        final page = MyHomePage(settings.arguments);
        return MaterialPageRoute(builder: (context) => page);

      default:
        return MaterialPageRoute(
            builder: (context) => MyHomePage(settings.arguments));
    }
  }
}