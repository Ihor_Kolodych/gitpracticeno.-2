import 'package:flutter/cupertino.dart';

class TextProvider with ChangeNotifier {


  int _sum = 3;

  int get Sum {
    return _sum;
  }

  void changeCounterList () {
    _sum++;
    notifyListeners();
  }

  void changeDecrimentList () {
    if(_sum<=0)
    {
      _sum=0;
      print('_sum < 0');
      notifyListeners();
    }else
    _sum--;
    notifyListeners();
  }

}

