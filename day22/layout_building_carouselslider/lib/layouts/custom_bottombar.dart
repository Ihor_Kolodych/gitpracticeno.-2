import 'package:carousel_slider/carousel_controller.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';

class CustomBottomBar extends StatefulWidget {
  @override
  _CustomBottomBarState createState() => _CustomBottomBarState();
}

class _CustomBottomBarState extends State<CustomBottomBar> {
  CarouselController buttonCarouselController = CarouselController();

  int index;
  int currentState;
  bool _visible = true;

  @override
  Widget build(BuildContext context) => ListView(children: <Widget>[
        Container(
          child: CarouselSlider(
            options: CarouselOptions(
pageSnapping: false,
                enableInfiniteScroll: true,
                height: 100,
                viewportFraction: 0.2,
                onPageChanged: (index, a) {
                  setState(() {
                    currentState = index;
                  });
                }),
            items: [1, 2, 3, 4, 5].map((i) {
              return Builder(
                builder: (BuildContext context) {
                  return Column(
                    children: [
                      Container(
                        height: MediaQuery.of(context).size.height * 0.14,
                        width: MediaQuery.of(context).size.width,
                        margin: EdgeInsets.symmetric(horizontal: 5.0),
                        decoration: BoxDecoration(
                          color: Colors.black26,
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        child: Icon(
                          Icons.android_outlined,
                          color: currentState == i - 1 ? Colors.yellowAccent : Colors.white,
                          size: 30.0,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 5.0),
                        child: AnimatedOpacity(
                          opacity: currentState == i - 1 ? 1.0 : 0.0,
                          duration: Duration(milliseconds: 1000),
                          child: Container(
                            decoration: BoxDecoration(
                                color: currentState == i - 1 ? Colors.yellowAccent : Colors.white.withOpacity(0),
                                borderRadius: BorderRadius.circular(10.0),
                                border: currentState == i - 1
                                    ? Border(
                                        top: BorderSide(width: 0.5, color: Colors.black),
                                        bottom: BorderSide(width: 0.5, color: Colors.black),
                                        left: BorderSide(width: 0.5, color: Colors.black),
                                        right: BorderSide(width: 0.5, color: Colors.black),
                                      )
                                    : Border()),
                            height: 5,
                            width: 20,
                          ),
                        ),
                      ),
                    ],
                  );
                },
              );
            }).toList(),
          ),
        ),
      ]);
}
