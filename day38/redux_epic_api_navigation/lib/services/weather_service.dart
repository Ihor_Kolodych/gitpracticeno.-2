import 'dart:convert';

import 'package:redux_epic_api_navigation/dto/weather_dto/weather_dto.dart';
import 'package:redux_epic_api_navigation/services/base_network_service.dart';
import 'package:redux_epic_api_navigation/services/network_service.dart';

class WeatherService {
  WeatherService._privateConstructor();

  static final WeatherService instance =
  WeatherService._privateConstructor();
  static String _url = 'https://api.openweathermap.org/data/2.5/forecast?q=Kherson&appid=86f68728cbe4671b38728a2bb6226b74';

  Future<BaseNetworkService> getWeather() async {

    BaseNetworkService responseData = await NetworkService.instance.request(HttpType.get, _url, null, null);

    WeatherDto weatherDto = WeatherDto.fromJson(responseData.response);
    print(weatherDto);

    return responseData;
    //WeatherDto weatherDtos = WeatherDto.fromJson(forEch)
    // var responseData =
    // await NetworkService.instance.request(HttpType.get, _url, null, null);
    // print(responseData.response.toString());
    // return responseData;
  }
}
