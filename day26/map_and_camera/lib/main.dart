import 'dart:async';
import 'package:flutter/material.dart';
import 'package:camera/camera.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'dart:io';

import 'package:location/location.dart';


Completer<GoogleMapController> _controller = Completer();

final CameraPosition _kGooglePlex = CameraPosition(
  target: LatLng(46.47766, 30.7398567),
  zoom: 14.4746,
);


final CameraPosition _kLake = CameraPosition(
    bearing: 192.8334901395799, target: LatLng(46.47766, 30.7398567), tilt: 59.440717697143555, zoom: 19.151926040649414);

List<CameraDescription> cameras;

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  cameras = await availableCameras();
  runApp(CameraApp());
}

class CameraApp extends StatefulWidget {
  @override
  _CameraAppState createState() => _CameraAppState();
}

class _CameraAppState extends State<CameraApp> {
  CameraController controller;
  String path;

  void takePic() {
    controller.takePicture().then((value) {
      path = value.path;
      setState(() {});
    });
  }

  @override
  void initState() {
    super.initState();
    controller = CameraController(cameras[0], ResolutionPreset.max);
    controller.initialize().then((_) {
      if (!mounted) {
        return;
      }
      setState(() {});
    });
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }

  int currentPage = 0;
  bool _previewImageUrl;
  double a;
  double b;
  @override
  Widget build(BuildContext context) {
    Future<void> _getCurrentUserLocation() async {
      final locData = await Location().getLocation();
      setState(() {
        a = locData.latitude;
        b = locData.longitude;
        _previewImageUrl = false;
      });
      print(locData.latitude);
      print(locData.longitude);
    }
    if (!controller.value.isInitialized) {
      return Container();
    }
    return MaterialApp(
      home: Scaffold(
        body: PageView(
          onPageChanged: (value) {
            setState(() {
              currentPage = value;
            });
          },
          scrollDirection: Axis.horizontal,
          children: [
            Scaffold(
              body: GoogleMap(
                mapType: MapType.hybrid,
                initialCameraPosition: _kGooglePlex,
                onMapCreated: (GoogleMapController controller) {
                  _controller.complete(controller);
                },
              ),
              floatingActionButton: FloatingActionButton(onPressed: () {_getCurrentUserLocation();},),
            ),
            SingleChildScrollView(
              child: Stack(
                children: [
                  Positioned(height: 400, width: 600, child: CameraPreview(controller)),
                  path != null
                      ? SizedBox(height: 400.0, width: 600, child: Image.file(File(path)))
                      : Positioned(child: SizedBox(height: 400, width: 600, child: CameraPreview(controller))),
                  Positioned(
                      bottom: 10,
                      right: 10,
                      child: FloatingActionButton(onPressed: () {

                        takePic();
                      })),
                  Positioned(
                      bottom: 10,
                      left: 10,
                      child: FloatingActionButton(onPressed: () {
                        setState(() {
                          path = null;
                        });
                      })),
                ],
              ),
            ),

          ],
        ),
      ),
    );
  }
}
