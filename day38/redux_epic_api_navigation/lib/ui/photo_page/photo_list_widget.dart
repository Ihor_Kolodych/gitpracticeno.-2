import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux_epic_api_navigation/store/application/app_state.dart';

import 'photo_page_vm.dart';
import 'photo_recipe.dart';

class PhotoRecipesListWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, PhotoPageVM>(
      converter: PhotoPageVM.fromStore,
      onInitialBuild: (vm) => vm.getRecipes(),
      builder: (BuildContext context, PhotoPageVM vm) {
        return vm.recipesList.isNotEmpty
            ? Scaffold(
                body: ListView(
                  itemExtent: 144.0,
                  children: [
                    ...vm.recipesList.map(
                      (recipe) {
                        return PhotoElement(
                          photo: recipe,
                        );
                      },
                    ).toList(),
                  ],
                ),
                floatingActionButton: FloatingActionButton(
                  onPressed: () => vm.goToWeather(),
                ),
              )
            : Container();
      },
    );
  }
}
