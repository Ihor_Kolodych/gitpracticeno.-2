// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'weather_main_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;
WeatherMainDto _$WeatherMainDtoFromJson(Map<String, dynamic> json) {
  return _WeatherMainDto.fromJson(json);
}

/// @nodoc
class _$WeatherMainDtoTearOff {
  const _$WeatherMainDtoTearOff();

// ignore: unused_element
  _WeatherMainDto call({@JsonKey(name: 'temp') num temp}) {
    return _WeatherMainDto(
      temp: temp,
    );
  }

// ignore: unused_element
  WeatherMainDto fromJson(Map<String, Object> json) {
    return WeatherMainDto.fromJson(json);
  }
}

/// @nodoc
// ignore: unused_element
const $WeatherMainDto = _$WeatherMainDtoTearOff();

/// @nodoc
mixin _$WeatherMainDto {
  @JsonKey(name: 'temp')
  num get temp;

  Map<String, dynamic> toJson();
  @JsonKey(ignore: true)
  $WeatherMainDtoCopyWith<WeatherMainDto> get copyWith;
}

/// @nodoc
abstract class $WeatherMainDtoCopyWith<$Res> {
  factory $WeatherMainDtoCopyWith(
          WeatherMainDto value, $Res Function(WeatherMainDto) then) =
      _$WeatherMainDtoCopyWithImpl<$Res>;
  $Res call({@JsonKey(name: 'temp') num temp});
}

/// @nodoc
class _$WeatherMainDtoCopyWithImpl<$Res>
    implements $WeatherMainDtoCopyWith<$Res> {
  _$WeatherMainDtoCopyWithImpl(this._value, this._then);

  final WeatherMainDto _value;
  // ignore: unused_field
  final $Res Function(WeatherMainDto) _then;

  @override
  $Res call({
    Object temp = freezed,
  }) {
    return _then(_value.copyWith(
      temp: temp == freezed ? _value.temp : temp as num,
    ));
  }
}

/// @nodoc
abstract class _$WeatherMainDtoCopyWith<$Res>
    implements $WeatherMainDtoCopyWith<$Res> {
  factory _$WeatherMainDtoCopyWith(
          _WeatherMainDto value, $Res Function(_WeatherMainDto) then) =
      __$WeatherMainDtoCopyWithImpl<$Res>;
  @override
  $Res call({@JsonKey(name: 'temp') num temp});
}

/// @nodoc
class __$WeatherMainDtoCopyWithImpl<$Res>
    extends _$WeatherMainDtoCopyWithImpl<$Res>
    implements _$WeatherMainDtoCopyWith<$Res> {
  __$WeatherMainDtoCopyWithImpl(
      _WeatherMainDto _value, $Res Function(_WeatherMainDto) _then)
      : super(_value, (v) => _then(v as _WeatherMainDto));

  @override
  _WeatherMainDto get _value => super._value as _WeatherMainDto;

  @override
  $Res call({
    Object temp = freezed,
  }) {
    return _then(_WeatherMainDto(
      temp: temp == freezed ? _value.temp : temp as num,
    ));
  }
}

@JsonSerializable(
    fieldRename: FieldRename.snake, checked: true, explicitToJson: true)

/// @nodoc
class _$_WeatherMainDto implements _WeatherMainDto {
  const _$_WeatherMainDto({@JsonKey(name: 'temp') this.temp});

  factory _$_WeatherMainDto.fromJson(Map<String, dynamic> json) =>
      _$_$_WeatherMainDtoFromJson(json);

  @override
  @JsonKey(name: 'temp')
  final num temp;

  @override
  String toString() {
    return 'WeatherMainDto(temp: $temp)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _WeatherMainDto &&
            (identical(other.temp, temp) ||
                const DeepCollectionEquality().equals(other.temp, temp)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(temp);

  @JsonKey(ignore: true)
  @override
  _$WeatherMainDtoCopyWith<_WeatherMainDto> get copyWith =>
      __$WeatherMainDtoCopyWithImpl<_WeatherMainDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_WeatherMainDtoToJson(this);
  }
}

abstract class _WeatherMainDto implements WeatherMainDto {
  const factory _WeatherMainDto({@JsonKey(name: 'temp') num temp}) =
      _$_WeatherMainDto;

  factory _WeatherMainDto.fromJson(Map<String, dynamic> json) =
      _$_WeatherMainDto.fromJson;

  @override
  @JsonKey(name: 'temp')
  num get temp;
  @override
  @JsonKey(ignore: true)
  _$WeatherMainDtoCopyWith<_WeatherMainDto> get copyWith;
}
