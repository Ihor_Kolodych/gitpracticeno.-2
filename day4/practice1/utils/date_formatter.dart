import 'package:intl/intl.dart';

class DateFormatDone {
  static String d(DateTime dateTime) {
    return DateFormat.yMMMd().format(dateTime);
  }
}
