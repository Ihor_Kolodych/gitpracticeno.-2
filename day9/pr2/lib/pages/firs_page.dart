import 'package:flutter/material.dart';
import 'package:untitled7/widgets/custom_inkwell.dart';

import '../genetarateRoute.dart';

class FirstPage extends StatelessWidget {
  final String argument;

  const FirstPage(this.argument);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('First Screen'),
      ),
      body: Center(
        child: Column(
          children: [
            CustomButton(
              onTap: () {
                Navigator.of(context).pushNamed(RouteGenerater.ROUTE_SECOND, arguments: 'Second page');
              },
              child: Text('Second page'),
            ),
            CustomButton(
              onTap: () {
                Navigator.of(context).pushNamed(RouteGenerater.ROUTE_MAIN, arguments: 'Main');
              },
              child: Text('Main'),
            ),
            CustomButton(
              onTap: () {
                Navigator.pop(context);
              },
              child: Text('Last page'),
            ),
          ],
        ),
      ),
    );
  }
}