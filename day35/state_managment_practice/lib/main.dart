import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:state_managment_practice/store/application/app_state.dart';
import 'package:state_managment_practice/store/settings_page_vm.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final store = Store<AppState>(
    AppState.getReducer,
    initialState: AppState.initial(),
  );

  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
      store: store,
      child: MaterialApp(
        home: MyHomePage(),
      ),
    );
  }
}

List<Color> color = [
  Colors.red,
  Colors.green,
  Colors.blue,
  Colors.yellow,
];

class MyHomePage extends StatelessWidget {
  Color darkGray = Color(0xFFa9a9a9);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, SettingsPageViewModel>(
      converter: SettingsPageViewModel.fromStore,
      builder: (BuildContext context, SettingsPageViewModel vm) {
        return SafeArea(
          child: Scaffold(
            backgroundColor: vm.color,
            drawer: Drawer(
              child: ListView.builder(
                physics: AlwaysScrollableScrollPhysics(),
                itemCount: color.length,
                itemBuilder: (BuildContext ctx , int index) {
                  return Container(
                    height: 200.0,
                    width: double.infinity,
                    color: color[index],
                    child: InkWell(
                      onTap: () {
                        vm.changeColorBackground(color[index]);
                      },
                    ),
                  );
                },
              ),
            ),
            appBar: AppBar(),
          ),
        );
      },
    );
  }
}
