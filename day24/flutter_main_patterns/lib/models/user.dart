class User {
  String name;
  int id;
  String email;
  int phone;

  User({this.name, this.id, this.email, this.phone});

  User copyWith({
    String name,
    int id,
    String email,
    int phone,
  }) {
    return User(
      name: name ?? this.name,
      id: id ?? this.id,
      email: email ?? this.email,
      phone: phone ?? this.phone,
    );
  }
}
