import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:redux_counter/counter_page_view_model.dart';
import 'package:redux_counter/store/app/app_state.dart';

void main() {
  Store store = Store<AppState>(
    AppState.getAppReducer,
    initialState: AppState.initial(),
  );
  runApp(MyApp(
    store: store,
  ));
}

class MyApp extends StatelessWidget {
  final Store<AppState> store;

  MyApp({@required this.store});

  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
      store: store,
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: MyHomePage(
          title: 'Redux',
        ), //PhotoPage(), //MyHomePage(title: 'Flutter Demo Home Page'),
      ),
    );
  }
}

class MyHomePage extends StatelessWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, CounterPageViewModel>(
      converter: CounterPageViewModel.fromStore,
      builder: (BuildContext context, CounterPageViewModel vm) {
        return Scaffold(
          appBar: AppBar(
            title: Text(title),
          ),
          body: Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Container(
                  height: 100,
                  width: 100,
                  child: Material(
                    color: Colors.blue,
                    child: InkWell(
                      onTap: vm.decrementCounter,
                      child: Center(
                        child: Text(
                          '-',
                          style: TextStyle(
                            color: Colors.yellow,
                            fontSize: 50.0,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                Text(
                  '${vm.counter.toString()}',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 50.0,
                  ),
                ),
                Container(
                  height: 100,
                  width: 100,
                  child: Material(
                    color: Colors.yellow,
                    child: InkWell(
                      onTap: vm.incrementCounter,
                      child: Center(
                        child: Text(
                          '+',
                          style: TextStyle(
                            color: Colors.blue,
                            fontSize: 50.0,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
