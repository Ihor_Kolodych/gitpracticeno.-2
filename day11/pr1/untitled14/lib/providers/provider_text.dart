import 'package:flutter/cupertino.dart';

class TextProvider with ChangeNotifier {
  String _TextSome = 'Initial text';

  String get TextSome {
    return _TextSome;
  }


  List<String> _listOfStrings = [
    'Button 1',
    'Button 2',
    'Button 3',
    'Button 4',
  ];

  List<String> get listOfStrings {
    return [..._listOfStrings];
  }

  void changeText (String value) {
    _TextSome = value;
    notifyListeners();
  }
}

