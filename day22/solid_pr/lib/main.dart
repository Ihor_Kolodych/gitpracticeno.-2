import 'dart:math';

import "package:flutter/material.dart";

void main() {
  runApp(MyApp(new AreaCalculator([])));
}

class AreaCalculatorExtend {
  void sum(List<dynamic> shapesTakeArea) {
    shapesTakeArea.forEach((element) {
      print(element.area().toString());
    });
  }
}

class AreaCalculator extends AreaCalculatorExtend {
  final List<dynamic> shapes;

  AreaCalculator(this.shapes);

  void output(List<dynamic> shapesForOutput) {
    shapesForOutput.forEach((element) {
      print(element.toString());
    });
  }
}

class Logic {
  void doSomething() {
    print("doing something");
  }
}

class Circle extends Shape {
  final double radius;

  Circle(this.radius);

  @override
  double area() {
    return radius * radius * pi;
  }
}

abstract class Shape {
  double area() {
    return 0.0;
  }
}

abstract class SomeAbstractClass {
  double someAbstractFunction() {
    return 0.0;
  }
}

class SomeClass extends SomeAbstractClass {
  @override
  double someAbstractFunction() {
    return 20.0;
  }
}

class OtherSomeClass extends SomeAbstractClass {
  @override
  double someAbstractFunction() {
    return 30.0;
  }
}

class Square extends Shape {
  final double length;

  Square(this.length);

  @override
  double area() {
    return length * length;
  }
}

class MyApp extends StatelessWidget {
  final AreaCalculator areaCalculator;

  MyApp(this.areaCalculator);

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: new HomePage(areaCalculator),
    );
  }
}

class HomePage extends StatelessWidget {
  final AreaCalculator areaCalculator;

  HomePage(this.areaCalculator);

  @override
  Widget build(BuildContext context) {
    List<dynamic> listData = [Circle(20.0), Square(20.0)];
    return Scaffold(
      body: Center(
        child: FlatButton(
          onPressed: () {
            areaCalculator.output(listData);
            areaCalculator.sum(listData);
          },
          child: Text("Print"),
        ),
      ),
    );
  }
}
