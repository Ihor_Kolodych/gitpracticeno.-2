import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:redux_navigation/store/shared/reducer.dart';
import 'package:redux_navigation/store/shared/settings_page_state/settings_page_action.dart';

class SettingsPageState {
  final Color color;
  final num counter;
  final num divideCounter;
  final num multipleCounter;


  SettingsPageState({
    this.color,
    this.counter,
    this.divideCounter,
    this.multipleCounter,
  });

  factory SettingsPageState.initial() {
    return SettingsPageState(
      color: Colors.white,
      counter: 2.0,
      divideCounter: 2.0,
      multipleCounter: 2.0,
    );
  }

  SettingsPageState copyWith({
    num counter,
    num divideCounter,
    num multipleCounter,
    Color color,
  }) {
    return SettingsPageState(
      counter: counter ?? this.counter,
      divideCounter: divideCounter ?? this.divideCounter,
      multipleCounter: multipleCounter ?? this.multipleCounter,
      color: color ?? this.color,
    );
  }

  SettingsPageState reducer(dynamic action) {
    return Reducer<SettingsPageState>(
      actions: HashMap.from({
        IncrementAction: (dynamic action) => _counter(),
        //IncrementAction: (dynamic action) => _divideCounter(),
        //IncrementAction: (dynamic action) => _multipleCounter(),
        ChangeColorBackgroundAction: (dynamic action) => _changeColorBackground(action as ChangeColorBackgroundAction),
        DecrementAction: (dynamic action) => _decrement(),
      }),
    ).updateState(action, this);
  }

  SettingsPageState _counter() {
    return copyWith(counter: counter + 1, divideCounter: divideCounter / 2, multipleCounter: multipleCounter * 10);

  }
  SettingsPageState _decrement() {
    return copyWith(counter: counter - 1, divideCounter: divideCounter / 2, multipleCounter: multipleCounter * 10);

  }
  // SettingsPageState _divideCounter() {
  //   return copyWith(counter: counter + 1);
  //
  // }
  // SettingsPageState _multipleCounter() {
  //   return copyWith(counter: counter + 1);
  // }

  SettingsPageState _changeColorBackground(ChangeColorBackgroundAction action){
    return copyWith(color: action.color);
  }
}