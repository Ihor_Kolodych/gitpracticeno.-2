import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:state_managment_practice/store/shared/settings_page_state/settings_page_selectors.dart';

import 'application/app_state.dart';

class SettingsPageViewModel {
  Color color;
  final void Function(Color color) changeColorBackground;

  SettingsPageViewModel({
    @required this.color,
    @required this.changeColorBackground,
  });

  static SettingsPageViewModel fromStore(Store<AppState> store) {
    return SettingsPageViewModel(
      changeColorBackground: SettingsPageSelectors.getChangeColorBackground(store),
      color: SettingsPageSelectors.getColorBackground(store),
    );
  }
}
