import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:redux_epic_api_navigation/dto/weather_main_dto/weather_main_dto.dart';

part 'weather_main_dto.freezed.dart';

part 'weather_main_dto.g.dart';

@freezed
abstract class WeatherMainDto with _$WeatherMainDto {
  @JsonSerializable(
    fieldRename: FieldRename.snake,
    checked: true,
    explicitToJson: true,
  )
  const factory WeatherMainDto({
    @JsonKey(name: 'temp') num temp,


  }) = _WeatherMainDto;

  factory WeatherMainDto.fromJson(Map<String, dynamic> json) => _$WeatherMainDtoFromJson(json);
}