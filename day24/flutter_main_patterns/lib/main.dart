import 'package:flutter/material.dart';
import 'package:flutter_main_patterns/adapters/adapter.dart';
import 'package:flutter_main_patterns/helpers/my_builder_helper.dart';
import 'package:flutter_main_patterns/helpers/singleton_helper.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  ValueNotifier<String> value = ValueNotifier<String>("123");
  OldRectangle oldRectangle = OldRectangle(1, 2, 200, 200);
  Rect rect = Rect(10, 10, 20, 20);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[

            ValueListenableBuilder<String>(
              valueListenable: value,
              builder: (context, String value, _) {
                return Text('Value is $value', style: AppFonts.textBold.copyWith(fontSize: 20),);
              },
            ),
            Builder(builder: (ctx2) => Container(height: MediaQuery.of(ctx2).size.height * 0.25,color: Colors.yellow,),),
            FloatingActionButton(onPressed: () {
              var s1 = Singleton();
              var s2 = Singleton();
              print(identical(s1, s2));
              print(s1 == s2);
              value.value = '125';
              rect.render();
              oldRectangle.render();
            })
          ],
        ),
      ),
    );
  }
}
