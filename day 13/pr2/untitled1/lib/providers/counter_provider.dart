import 'package:flutter/cupertino.dart';

class TextProvider with ChangeNotifier {


  int _sum = 0;

  int get Sum {
    return _sum;
  }

  void changeCounterList () {
    _sum++;
    notifyListeners();
  }

  void changeDecrimentList () {
      _sum--;
    notifyListeners();
  }

}