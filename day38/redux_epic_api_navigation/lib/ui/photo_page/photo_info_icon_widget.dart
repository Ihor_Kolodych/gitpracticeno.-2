import 'package:flutter/material.dart';

class PhotoInfoIconWidget extends StatelessWidget {
  final IconData icon;
  final String text;

  const PhotoInfoIconWidget(this.icon, this.text);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Icon(
          icon,
          size: 20.0,
          color: Colors.yellow,
        ),
        const SizedBox(
          width: 4.0,
        ),
        Text(
          text,
        ),
      ],
    );
  }
}
