import 'package:flutter/material.dart';

class AppBarLayout extends PreferredSize {
  AnimationController controller;
  double size;
  double shadow;

  AppBarLayout(this.controller, this.size, this.shadow);

  Size get preferredSize => Size.fromHeight(400);

  @override
  Widget build(BuildContext context) {
    return PreferredSize(
      preferredSize: Size.fromHeight(100),
      child: SafeArea(
        child: AnimatedBuilder(
          animation: controller,
          builder: (context, child) => Stack(
            children: <Widget>[
              Transform.translate(
                offset: Offset(0, -controller.value),
                child: Container(
                  height: size,
                  child: Container(
                    decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(1),
                          blurRadius: shadow,
                          offset: Offset(0, shadow),
                        ),
                      ],
                    ),
                    child: AppBar(
                      title: Text('Title'),
                      leading: Icon(
                        Icons.arrow_back,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
