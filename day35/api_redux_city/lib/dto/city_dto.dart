import 'package:flutter/foundation.dart';

class CityDto {
  final String id;
  final String city;
  final String state;
  final String country;

  CityDto({
    @required this.id,
    @required this.city,
    @required this.state,
    @required this.country,
  });

  factory CityDto.fromJson(Map<String, dynamic> json) {
    return CityDto(
      id: json["id"].toString(),
      city: json["city"].toString(),
      state: json["state"].toString(),
      country: json["country"].toString(),
    );
  }
}
