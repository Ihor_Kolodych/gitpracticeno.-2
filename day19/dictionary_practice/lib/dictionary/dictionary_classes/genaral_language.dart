import 'package:flutter/cupertino.dart';

class GeneralLanguage {
  final String text;

  const GeneralLanguage({
    @required this.text,
  });

  factory GeneralLanguage.fromJson(Map<String, dynamic> json) {
    return GeneralLanguage(
      text: json['text'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'text': text,
    };
  }
}