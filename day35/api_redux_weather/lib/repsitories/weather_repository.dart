import 'package:api_redux_weather/dto/openweather_dto.dart';
import 'package:api_redux_weather/services/weather_service.dart';

class WeatherRepository {
  WeatherRepository._privateConstructor();

  static final WeatherRepository instance = WeatherRepository._privateConstructor();

  Future<OpenWeatherDto> getImages() async {
    var responseData = await WeatherService.instance.getImages();
    OpenWeatherDto weather = OpenWeatherDto.fromJson(responseData);

    return weather;
  }
}
