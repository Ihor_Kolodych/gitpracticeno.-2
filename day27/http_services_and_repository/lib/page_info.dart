import 'package:flutter/material.dart';

class PageInfo extends StatelessWidget {
  String url;
  String price;
  String name;
  int index;
  String description;

  PageInfo({this.url, this.price, this.name, this.index, this.description});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: InkWell(
        onTap: () {
          Navigator.of(context).pop();
        },
        child: Hero(
          tag: index,
          child: Material(
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Container(
                    width: 100,
                    height: 100,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        fit: BoxFit.fill,
                        image: NetworkImage(url),
                      ),
                    ),
                    child: Align(
                      alignment: Alignment.bottomRight,
                      child: Container(
                        color: Colors.yellow,
                        child: Text(
                          'Price: ${price}\$',
                        ),
                      ),
                    ),
                  ),
                  FittedBox(
                    fit: BoxFit.fill,
                    child: Align(
                      alignment: Alignment.bottomCenter,
                      child: Text(
                        '${name}',
                      style: TextStyle(fontStyle: FontStyle.italic),
                      ),
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(9.0),
                      border: Border.all(width: 1.0, color: Colors.black),
                    ),
                    child: Align(
                      alignment: Alignment.bottomCenter,
                      child: Text(
                        '${description}',
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
