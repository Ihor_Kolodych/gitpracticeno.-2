import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux_navigation/main.dart';
import 'package:redux_navigation/store/application/app_state.dart';
import 'package:redux_navigation/store/settings_page_vm.dart';

class Page3 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, SettingsPageViewModel>(
      converter: SettingsPageViewModel.fromStore,
      builder: (BuildContext context, SettingsPageViewModel vm) {
        return MyHomePage(
          child: Center(
            child: Text(
              vm.multiple.toString(),
              style: TextStyle(fontSize: 40),
            ),
          ),
          floatingActionButton: FloatingActionButton(
            child: Icon(
              Icons.add,
            ),
            onPressed: vm.incrementCounter,
          ),
        );
      },
    );
  }
}
