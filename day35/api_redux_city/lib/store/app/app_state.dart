import 'package:api_redux_city/store/pages/photos_state/city_epics.dart';
import 'package:api_redux_city/store/pages/photos_state/city_state.dart';
import 'package:flutter/foundation.dart';

import 'package:redux_epics/redux_epics.dart';

class AppState {
  final CityState weatherState;


  AppState({
    @required this.weatherState,
  });

  factory AppState.initial() {
    return AppState(
      weatherState: CityState.initial(),
    );
  }

  static AppState getAppReducer(AppState state, dynamic action) {
    const String TAG = '[appReducer]';
    print('$TAG => <appReducer> => action: ${action.runtimeType}');
    return AppState(
      weatherState: state.weatherState.reducer(action),
    );
  }

  static final getAppEpic = combineEpics<AppState>([
    CityEpics.indexEpic,
  ]);
}