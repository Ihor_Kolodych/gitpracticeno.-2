import 'package:flutter/material.dart';
import 'package:untitled16/layouts/appbar_layout.dart';
import 'package:untitled16/layouts/drawer_layout.dart';

class FirstPage extends StatelessWidget {
  final String argument;

  const FirstPage(this.argument);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawerEnableOpenDragGesture: false,
      backgroundColor: Colors.lightGreen,
      body: Text('First Page'),
      appBar: AppBarLayout(),
      drawer: DrawerLayout(),
    );
  }
}
