import 'package:flutter/material.dart';

class CustomContainer extends StatelessWidget {

  Color color;
  Widget child;
  CustomContainer({this.color, this.child});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(2),
      width: 100,
      height: 50,
      color: color,
      child: Center(child: child),
    );
  }
}
