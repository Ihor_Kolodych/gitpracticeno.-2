
import 'dart:convert';

import 'package:redux_epic_api_navigation/services/base_network_service.dart';
import 'package:redux_epic_api_navigation/services/network_service.dart';

class PhotoService {
  PhotoService._privateConstructor();

  static final PhotoService instance =
  PhotoService._privateConstructor();
  static String _url = 'https://picsum.photos/v2/list';

  Future<BaseNetworkService> getPhoto() async {
    var responseData =
    await NetworkService.instance.request(HttpType.get, _url, null, null);
    print(responseData.response);
    return responseData;
  }
}
