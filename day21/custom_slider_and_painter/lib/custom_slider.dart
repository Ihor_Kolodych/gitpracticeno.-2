import 'package:custom_slider_and_painter/my_painter.dart';
import 'package:flutter/material.dart';

class CustomSlider extends StatefulWidget {
  final double max;
  final double min;
  final double initialValue;
  final double widthSlider;

  CustomSlider({
    this.min,
    this.max,
    this.initialValue,
    this.widthSlider,
  });

  @override
  _CustomSliderState createState() => _CustomSliderState();
}

class _CustomSliderState extends State<CustomSlider> {
  double valueInitial = 0.0;
  @override
  void initState() {
    setState(() {
      valueInitial = (widget.widthSlider / (widget.max - widget.min)) * (widget.initialValue - widget.min);
    });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    double widthSlider = MediaQuery.of(context).size.width * 0.75;
    double heightSlider = MediaQuery.of(context).size.width * 0.05;
    double percentageSlider = ((widget.max-widget.min) / widthSlider * valueInitial)+widget.min;
    double percentageSliderMin = ((widget.max-widget.min) / widthSlider * valueInitial);
    return SingleChildScrollView(
      child: Column(
        children: [
          Text(
            percentageSlider.round().toString(),
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 70.0,
            ),
          ),
          GestureDetector(
            onHorizontalDragUpdate: (DragUpdateDetails dragUpdateDetails) {
              double distance = dragUpdateDetails.localPosition.dx;
              double percentageAddition = distance / 100;
              setState(() {
                valueInitial = ((dragUpdateDetails.localPosition.dx) + percentageAddition).clamp(percentageSliderMin, widthSlider);
              });
            },
            onTapDown: (details) {
              double distance = details.localPosition.dx;
              double percentageAddition = distance / 100;
              setState(() {
                valueInitial = (details.localPosition.dx + percentageAddition).clamp(percentageSliderMin, widthSlider);
              });
            },
            child: Container(
              height: heightSlider,
              width: widthSlider,
              child: CustomPaint(
                painter: CustomSliderPainter(valueInitial: valueInitial),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
