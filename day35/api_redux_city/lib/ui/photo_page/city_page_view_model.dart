import 'package:api_redux_city/dto/city_dto.dart';
import 'package:api_redux_city/store/app/app_state.dart';
import 'package:api_redux_city/store/pages/photos_state/city_selector.dart';
import 'package:flutter/foundation.dart';
import 'package:redux/redux.dart';



class CityPageViewModel {
  final List<CityDto> weather;
  final void Function() getCity;

  CityPageViewModel({
    @required this.weather,
    @required this.getCity,
  });

  static CityPageViewModel fromStore(Store<AppState> store) {
    return CityPageViewModel(
      weather: CitysSelector.getCurrentCity(store),
      getCity: CitysSelector.getCitysFunction(store),
    );
  }
}
