import 'package:redux_epic_api_navigation/dto/photo_dto.dart';
import 'package:redux_epic_api_navigation/dto/weather_dto/weather_dto.dart';
import 'package:redux_epic_api_navigation/repsitories/photo_repository.dart';
import 'package:redux_epic_api_navigation/repsitories/weather_repository.dart';
import 'package:redux_epic_api_navigation/store/application/app_state.dart';
import 'package:redux_epic_api_navigation/store/shared/photo_state/photos_actions.dart';
import 'package:redux_epic_api_navigation/store/shared/weather_state/weather_actions.dart';
import 'package:redux_epics/redux_epics.dart';
import 'package:rxdart/rxdart.dart';

class WeatherEpics {
  static const String tag = '[AuthEpics]';

  static final indexEpic = combineEpics<AppState>([
    getWeatherEpic,
  ]);

  static Stream<dynamic> getWeatherEpic(Stream<dynamic> actions, EpicStore<AppState> store) {
    return actions.whereType<GetWeatherAction>().switchMap((GetWeatherAction action) async* {
      final WeatherDto a = await WeatherRepository.instance.getWeather();
      yield SetWeatherToStateAction(a);
    });
  }
}
