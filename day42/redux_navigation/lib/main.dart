import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:redux/redux.dart';
import 'package:redux_navigation/routs/route_helper.dart';
import 'package:redux_navigation/routs/routs.dart';
import 'package:redux_navigation/store/application/app_state.dart';
import 'package:redux_navigation/store/settings_page_vm.dart';

void main() {
  Store store = Store<AppState>(
    AppState.getReducer,
    initialState: AppState.initial(),
    middleware: [
      NavigationMiddleware<AppState>(),
    ],
  );
  runApp(MyApp(
    store: store,
  ));
}

class MyApp extends StatelessWidget {
  final Store<AppState> store;

  MyApp({@required this.store});

  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
      store: store,
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        navigatorKey: NavigatorHolder.navigatorKey,
        onGenerateRoute: (RouteSettings settings) => RouteHelper.instance.onGenerateRoute(settings),
        home: MyHomePage(), //PhotoPage(), //MyHomePage(title: 'Flutter Demo Home Page'),
      ),
    );
  }
}

List<Color> color = [
  Colors.white,
  Colors.green,
  Colors.blue,
  Colors.red,
];

List<String> route = [
  Routes.mainPage,
  Routes.page1,
  Routes.page2,
  Routes.page3,
];

class MyHomePage extends StatelessWidget {
  Widget child;
  FloatingActionButton floatingActionButton;

  MyHomePage({
    this.child,
    this.floatingActionButton,
  });

  Color darkGray = Color(0xFFa9a9a9);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, SettingsPageViewModel>(
      converter: SettingsPageViewModel.fromStore,
      builder: (BuildContext context, SettingsPageViewModel vm) {
        return SafeArea(
          child: Scaffold(
            backgroundColor: vm.color,
            drawer: Drawer(
                child: Column(
                  children: [
                    Container(
                      height: 100,
                      width: double.infinity,
                      child: InkWell(
                        onTap: vm.incrementCounter,
                        child: Center(
                          child: Text('increment'),
                        ),
                      ),
                    ),
                    Container(
                      height: 100,
                      width: double.infinity,
                      child: InkWell(
                        onTap: vm.decrementCounter,
                        child: Center(
                          child: Text('decrement'),
                        ),
                      ),
                    ),
                    ListView.builder(
                      //physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: color.length,
                      itemBuilder: (BuildContext ctx, int index) {
                        return Container(
                          height: 100.0,
                          width: double.infinity,
                          color: color[index],
                          child: InkWell(
                            onTap: () {
                              vm.changeColorBackground(color[index]);
                              vm.gotoPage(route[index]);
                            },
                            child: Center(child: Text(index == 0 ? 'MainPage' : 'Page$index')),
                          ),
                        );
                      },
                    ),
                  ],
                )),
            body: Center(child: child ?? Text('Empty Page')),
            appBar: AppBar(),
            floatingActionButton: floatingActionButton,
          ),
        );
      },
    );
  }
}
