import 'package:dictionary_practice/dictionary/data/en.dart';
import 'package:dictionary_practice/dictionary/dictionary_classes/genaral_language.dart';
import 'package:dictionary_practice/dictionary/dictionary_classes/setting_page_dictionary.dart';
import 'package:dictionary_practice/dictionary/flutter_delegate.dart';
import 'package:dictionary_practice/dictionary/flutter_dictionary.dart';
import 'package:dictionary_practice/dictionary/models/supported_locales.dart';
import 'package:dictionary_practice/providers/language_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class TextX extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    GeneralLanguage language = FlutterDictionary.instance.language?.generalLanguage ?? en.generalLanguage;
    SettingsPageDictionary languageTwo = FlutterDictionary.instance.language?.settingsPageDictionary ?? en.settingsPageDictionary;
    return Column(
      children: [
        Container(
          child: Center(
            child: Text(
                language.text
            ),
          ),
        ),
      ],
    );
  }
}