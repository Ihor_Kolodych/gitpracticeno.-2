import 'package:redux/redux.dart';
import 'package:redux_epic_api_navigation/dto/photo_dto.dart';
import 'package:redux_epic_api_navigation/store/application/app_state.dart';
import 'package:redux_epic_api_navigation/store/shared/photo_state/photos_actions.dart';

class PhotosSelectors{
  static void Function() getPhotoFunction(Store<AppState> store){
    return () => store.dispatch(GetPhotosAction());
  }
  static List<PhotoDto> photosList(Store<AppState> store){
    return store.state.photosState.photos;
  }
}
