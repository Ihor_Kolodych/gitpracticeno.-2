void main (){
  Azawakh azawakh = Azawakh();
  Aidi aidi = Aidi();
  Alabai alabai = Alabai();
  List<IDogBreed>dogs=[azawakh,aidi,alabai];
  listBreed(dogs);
  breedToName(alabai);
} 
  
 void tellDogBreed(IDogBreed dog){
   dog.tellBreed();
 }

  abstract class IDogBreed{
    String breedName = "none";
    tellBreed();
  }
    
  class Azawakh implements IDogBreed{
    String breedName = "Azawakh";
    int indexBreed = 1;
    @override
    void tellBreed(){
      print("$indexBreed $breedName");
    }
  }

  class Aidi implements IDogBreed{
    String breedName = "Aidi";
    int indexBreed = 2;
    @override
    void tellBreed(){
      print("$indexBreed $breedName");
    }
  }

  class Alabai implements IDogBreed{
    String breedName = "Alabai";
    int indexBreed = 4;
    @override
    void tellBreed(){
       print("$indexBreed $breedName");
    }
  }

 void listBreed(List<IDogBreed>dog)
  {
    for(int i=0; i<dog.length; i++){
      dog[i].tellBreed();
    }
  }

 void breedToName(IDogBreed dog)
  {
    dog.tellBreed();
  }
  
