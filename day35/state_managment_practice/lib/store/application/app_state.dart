import 'package:flutter/cupertino.dart';
import 'package:state_managment_practice/store/shared/settings_page_state/settings_page_state.dart';

/// Class [AppState], is the main [state] application.
/// It keeps 3, smaller states.
/// Namely, [dialogState], [storageState], [loaderState].
/// First [dialogState], this variable stores the state of dialogs, it is used to call various dialogs.
/// Second [storageState], the primary state, stores all information from all states.
/// The third [loaderState] is required to loading.
class AppState {
  final SettingsPageState settingsPageState;

  AppState({
    @required this.settingsPageState,
  });

  ///All states are initialized in the [initial] function.
  factory AppState.initial() {
    return AppState(
      settingsPageState: SettingsPageState.initial(),
    );
  }

  ///The [getReducer] function is the main Reducer in which you call Reducer, all other states.
  static AppState getReducer(AppState state, dynamic action) {
    return AppState(
      settingsPageState: state.settingsPageState.reducer(action),
    );
  }
}
