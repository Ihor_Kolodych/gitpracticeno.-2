import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:redux/redux.dart';
import 'package:redux_navigation/routs/routs.dart';
import 'package:redux_navigation/store/application/app_state.dart';

class RouteSelectors {
  static void Function(String) gotoPage(Store<AppState> store) {
    return (String routes) => store.dispatch(NavigateToAction.push(routes));
  }
  static void Function() popPage(Store<AppState> store) {
    return () => store.dispatch(NavigateToAction.replace(Routes.mainPage));
  }
}


