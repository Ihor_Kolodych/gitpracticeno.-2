import 'package:api_redux_weather/dto/openweather_dto.dart';

class GetImagesAction {
  GetImagesAction();
}

class SetInitImagesAction {
  final OpenWeatherDto images;

  SetInitImagesAction({this.images});
}

class SaveImagesAction {
  OpenWeatherDto weathers;

  SaveImagesAction({this.weathers});
}

class SelectImageAction {
  String id;

  SelectImageAction({this.id});
}
