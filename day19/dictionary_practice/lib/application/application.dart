import 'package:dictionary_practice/dictionary/flutter_delegate.dart';
import 'package:dictionary_practice/dictionary/models/supported_locales.dart';
import 'package:dictionary_practice/providers/language_provider.dart';
import 'package:dictionary_practice/ui/home_page.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Application extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<LanguageProvider>(
      builder: (BuildContext context, LanguageProvider provider, Widget _) {
        return MaterialApp(
          theme: ThemeData.dark(),
          debugShowCheckedModeBanner: false,
          locale: Locale(FlutterDictionaryDelegate.getCurrentLocale),
          supportedLocales: SupportedLocales.instance.getSupportedLocales,
          localizationsDelegates: FlutterDictionaryDelegate.getLocalizationDelegates,
          home: HomePage(),
        );
      },
    );
  }
}
