import 'package:flutter/foundation.dart';

class People {
  String id;
  String name;
  DateTime date;

  People({
    @required this.id,
    @required this.name,
    @required this.date,
  });
}
