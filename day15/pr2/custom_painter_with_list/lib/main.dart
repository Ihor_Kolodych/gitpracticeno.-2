import 'package:flutter/material.dart';
import 'dart:math' as math;

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class Painter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final radius = math.min(size.width, size.height) / 4;
    final center = Offset(size.width / 2, size.height / 2);
    final color = Paint()..color = Colors.yellow;

    canvas.drawCircle(center, radius, color);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    throw UnimplementedError();
  }
}

class _MyHomePageState extends State<MyHomePage> {
  final customPaint = CustomPaint(
    painter: Painter(),
  );

  bool isReversed = false;
  List<Widget> createList = List<Widget>.generate(
    101,
    (int index) => Text(
      '${index}',
      textAlign: TextAlign.center,
    ),
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Center(
            child: SizedBox(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: customPaint,
            ),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ElevatedButton(
                onPressed: () {
                  setState(() {
                    isReversed = !isReversed;
                  });
                },
                child: Text('Reversed'),
              ),
              Center(
                child: Container(
                  height: MediaQuery.of(context).size.height * 0.2,
                  width: MediaQuery.of(context).size.width * 0.2,
                  child: (ListView(
                    children: isReversed ? createList.reversed.toList() : createList,
                  )),
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
