import 'package:api_redux_weather/store/app/app_state.dart';
import 'package:api_redux_weather/ui/photo_page/weather_page_view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

class PhotoPage extends StatefulWidget {
  @override
  _PhotoPageState createState() => _PhotoPageState();
}

class _PhotoPageState extends State<PhotoPage> {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, WeatherPageViewModel>(
      converter: WeatherPageViewModel.fromStore,
      onInitialBuild: (WeatherPageViewModel viewModel) => viewModel.getWeather(),
      builder: (BuildContext context, WeatherPageViewModel viewModel) {
        return Material(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                Text(
                  'City: ${viewModel.weather.name}' ?? '',
                  textAlign: TextAlign.center,
                  maxLines: 2,
                ),
                Text(
                  'Country: ${viewModel.weather.country}' ?? '',
                  textAlign: TextAlign.center,
                  maxLines: 2,
                ),
                Text(
                  'Weather: ${viewModel.weather.descriptionWeather}' ?? '',
                  textAlign: TextAlign.center,
                  maxLines: 2,
                ),
                Text(
                  'Wind: ${viewModel.weather.wind}' ?? '',
                  textAlign: TextAlign.center,
                  maxLines: 2,
                ),
                  Text(
                    'Temp: ${viewModel.weather.temp}' ?? '',
                    textAlign: TextAlign.center,
                    maxLines: 2,
                  ),
              ],),

              FloatingActionButton(onPressed: viewModel.getWeather)
            ],
          ),
        );
      },
    );
  }
}
