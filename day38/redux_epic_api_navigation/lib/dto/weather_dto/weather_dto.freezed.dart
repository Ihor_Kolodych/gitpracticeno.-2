// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'weather_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;
WeatherDto _$WeatherDtoFromJson(Map<String, dynamic> json) {
  return _WeatherDto.fromJson(json);
}

/// @nodoc
class _$WeatherDtoTearOff {
  const _$WeatherDtoTearOff();

// ignore: unused_element
  _WeatherDto call(
      {@JsonKey(name: 'cod') String cod,
      @JsonKey(name: 'list') List<WeatherDetailDto> list,
      @JsonKey(name: 'city') WeatherCityDto city}) {
    return _WeatherDto(
      cod: cod,
      list: list,
      city: city,
    );
  }

// ignore: unused_element
  WeatherDto fromJson(Map<String, Object> json) {
    return WeatherDto.fromJson(json);
  }
}

/// @nodoc
// ignore: unused_element
const $WeatherDto = _$WeatherDtoTearOff();

/// @nodoc
mixin _$WeatherDto {
  @JsonKey(name: 'cod')
  String get cod;
  @JsonKey(name: 'list')
  List<WeatherDetailDto> get list;
  @JsonKey(name: 'city')
  WeatherCityDto get city;

  Map<String, dynamic> toJson();
  @JsonKey(ignore: true)
  $WeatherDtoCopyWith<WeatherDto> get copyWith;
}

/// @nodoc
abstract class $WeatherDtoCopyWith<$Res> {
  factory $WeatherDtoCopyWith(
          WeatherDto value, $Res Function(WeatherDto) then) =
      _$WeatherDtoCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: 'cod') String cod,
      @JsonKey(name: 'list') List<WeatherDetailDto> list,
      @JsonKey(name: 'city') WeatherCityDto city});

  $WeatherCityDtoCopyWith<$Res> get city;
}

/// @nodoc
class _$WeatherDtoCopyWithImpl<$Res> implements $WeatherDtoCopyWith<$Res> {
  _$WeatherDtoCopyWithImpl(this._value, this._then);

  final WeatherDto _value;
  // ignore: unused_field
  final $Res Function(WeatherDto) _then;

  @override
  $Res call({
    Object cod = freezed,
    Object list = freezed,
    Object city = freezed,
  }) {
    return _then(_value.copyWith(
      cod: cod == freezed ? _value.cod : cod as String,
      list: list == freezed ? _value.list : list as List<WeatherDetailDto>,
      city: city == freezed ? _value.city : city as WeatherCityDto,
    ));
  }

  @override
  $WeatherCityDtoCopyWith<$Res> get city {
    if (_value.city == null) {
      return null;
    }
    return $WeatherCityDtoCopyWith<$Res>(_value.city, (value) {
      return _then(_value.copyWith(city: value));
    });
  }
}

/// @nodoc
abstract class _$WeatherDtoCopyWith<$Res> implements $WeatherDtoCopyWith<$Res> {
  factory _$WeatherDtoCopyWith(
          _WeatherDto value, $Res Function(_WeatherDto) then) =
      __$WeatherDtoCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: 'cod') String cod,
      @JsonKey(name: 'list') List<WeatherDetailDto> list,
      @JsonKey(name: 'city') WeatherCityDto city});

  @override
  $WeatherCityDtoCopyWith<$Res> get city;
}

/// @nodoc
class __$WeatherDtoCopyWithImpl<$Res> extends _$WeatherDtoCopyWithImpl<$Res>
    implements _$WeatherDtoCopyWith<$Res> {
  __$WeatherDtoCopyWithImpl(
      _WeatherDto _value, $Res Function(_WeatherDto) _then)
      : super(_value, (v) => _then(v as _WeatherDto));

  @override
  _WeatherDto get _value => super._value as _WeatherDto;

  @override
  $Res call({
    Object cod = freezed,
    Object list = freezed,
    Object city = freezed,
  }) {
    return _then(_WeatherDto(
      cod: cod == freezed ? _value.cod : cod as String,
      list: list == freezed ? _value.list : list as List<WeatherDetailDto>,
      city: city == freezed ? _value.city : city as WeatherCityDto,
    ));
  }
}

@JsonSerializable(
    fieldRename: FieldRename.snake, checked: true, explicitToJson: true)

/// @nodoc
class _$_WeatherDto implements _WeatherDto {
  const _$_WeatherDto(
      {@JsonKey(name: 'cod') this.cod,
      @JsonKey(name: 'list') this.list,
      @JsonKey(name: 'city') this.city});

  factory _$_WeatherDto.fromJson(Map<String, dynamic> json) =>
      _$_$_WeatherDtoFromJson(json);

  @override
  @JsonKey(name: 'cod')
  final String cod;
  @override
  @JsonKey(name: 'list')
  final List<WeatherDetailDto> list;
  @override
  @JsonKey(name: 'city')
  final WeatherCityDto city;

  @override
  String toString() {
    return 'WeatherDto(cod: $cod, list: $list, city: $city)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _WeatherDto &&
            (identical(other.cod, cod) ||
                const DeepCollectionEquality().equals(other.cod, cod)) &&
            (identical(other.list, list) ||
                const DeepCollectionEquality().equals(other.list, list)) &&
            (identical(other.city, city) ||
                const DeepCollectionEquality().equals(other.city, city)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(cod) ^
      const DeepCollectionEquality().hash(list) ^
      const DeepCollectionEquality().hash(city);

  @JsonKey(ignore: true)
  @override
  _$WeatherDtoCopyWith<_WeatherDto> get copyWith =>
      __$WeatherDtoCopyWithImpl<_WeatherDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_WeatherDtoToJson(this);
  }
}

abstract class _WeatherDto implements WeatherDto {
  const factory _WeatherDto(
      {@JsonKey(name: 'cod') String cod,
      @JsonKey(name: 'list') List<WeatherDetailDto> list,
      @JsonKey(name: 'city') WeatherCityDto city}) = _$_WeatherDto;

  factory _WeatherDto.fromJson(Map<String, dynamic> json) =
      _$_WeatherDto.fromJson;

  @override
  @JsonKey(name: 'cod')
  String get cod;
  @override
  @JsonKey(name: 'list')
  List<WeatherDetailDto> get list;
  @override
  @JsonKey(name: 'city')
  WeatherCityDto get city;
  @override
  @JsonKey(ignore: true)
  _$WeatherDtoCopyWith<_WeatherDto> get copyWith;
}
