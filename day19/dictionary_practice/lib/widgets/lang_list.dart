import 'package:dictionary_practice/dictionary/flutter_delegate.dart';
import 'package:dictionary_practice/dictionary/models/supported_locales.dart';
import 'package:dictionary_practice/providers/language_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class LangList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 250.0,
      margin: EdgeInsets.all(10),
      child: Consumer<LanguageProvider>(
        builder: (BuildContext context, LanguageProvider provider, Widget child) => Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            for (Locale loc in SupportedLocales.instance.getSupportedLocales)
              InkWell(
                onTap: () => provider.setNewLane(loc),
                child: AnimatedContainer(
                  padding: EdgeInsets.all(15),
                  duration: Duration(milliseconds: 400),
                  child: Text(
                    loc.toString().toUpperCase(),
                    style: TextStyle(fontSize: 20),
                  ),
                ),
              ),
          ],
        ),
      ),
    );
  }
}
