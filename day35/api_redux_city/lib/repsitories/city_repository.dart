import 'package:api_redux_city/dto/city_dto.dart';
import 'package:api_redux_city/services/city_service.dart';

class CityRepository {
  CityRepository._privateConstructor();

  static final CityRepository instance =
      CityRepository._privateConstructor();

  Future<List<CityDto>> getCitys() async {
    var responseData = await CityService.instance.getCitys();
    List<CityDto> citys = [];
    for (var item in responseData.response) {
      citys.add(CityDto.fromJson(item));
    }
    return citys;
  }
}
