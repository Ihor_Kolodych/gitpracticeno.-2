import 'package:flutter/material.dart';

import 'appbar_layout.dart';

class MainLayout extends StatefulWidget {
  @override
  _MainLayoutState createState() => _MainLayoutState();
}

class _MainLayoutState extends State<MainLayout> with SingleTickerProviderStateMixin {
  AnimationController _controller;
  Animation<double> _size;
  Animation<double> _shadow;

  @override
  void initState() {
    _controller = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 300),
    );
    _size = Tween<double>(begin: 50, end: 100).animate(_controller);
    _shadow = Tween<double>(begin: 0, end: 40).animate(_controller);

    _controller.addListener(() {
      setState(() {});
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBarLayout(_controller, _size.value, _shadow.value),
      body: Center(
        child: Container(
          height: 50,
          width: 100,
          child: Material(
            color: Colors.lightGreen,
            child: InkWell(
              onTap: () {
                if (_controller.isCompleted) {
                  _controller.reverse();
                } else {
                  _controller.forward();
                }
              },
            ),
          ),
        ),
      ),
    );
  }
}
