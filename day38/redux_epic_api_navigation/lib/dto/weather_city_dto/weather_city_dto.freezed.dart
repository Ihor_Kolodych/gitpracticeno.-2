// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'weather_city_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;
WeatherCityDto _$WeatherCityDtoFromJson(Map<String, dynamic> json) {
  return _WeatherCityDto.fromJson(json);
}

/// @nodoc
class _$WeatherCityDtoTearOff {
  const _$WeatherCityDtoTearOff();

// ignore: unused_element
  _WeatherCityDto call({@JsonKey(name: 'name') String name}) {
    return _WeatherCityDto(
      name: name,
    );
  }

// ignore: unused_element
  WeatherCityDto fromJson(Map<String, Object> json) {
    return WeatherCityDto.fromJson(json);
  }
}

/// @nodoc
// ignore: unused_element
const $WeatherCityDto = _$WeatherCityDtoTearOff();

/// @nodoc
mixin _$WeatherCityDto {
  @JsonKey(name: 'name')
  String get name;

  Map<String, dynamic> toJson();
  @JsonKey(ignore: true)
  $WeatherCityDtoCopyWith<WeatherCityDto> get copyWith;
}

/// @nodoc
abstract class $WeatherCityDtoCopyWith<$Res> {
  factory $WeatherCityDtoCopyWith(
          WeatherCityDto value, $Res Function(WeatherCityDto) then) =
      _$WeatherCityDtoCopyWithImpl<$Res>;
  $Res call({@JsonKey(name: 'name') String name});
}

/// @nodoc
class _$WeatherCityDtoCopyWithImpl<$Res>
    implements $WeatherCityDtoCopyWith<$Res> {
  _$WeatherCityDtoCopyWithImpl(this._value, this._then);

  final WeatherCityDto _value;
  // ignore: unused_field
  final $Res Function(WeatherCityDto) _then;

  @override
  $Res call({
    Object name = freezed,
  }) {
    return _then(_value.copyWith(
      name: name == freezed ? _value.name : name as String,
    ));
  }
}

/// @nodoc
abstract class _$WeatherCityDtoCopyWith<$Res>
    implements $WeatherCityDtoCopyWith<$Res> {
  factory _$WeatherCityDtoCopyWith(
          _WeatherCityDto value, $Res Function(_WeatherCityDto) then) =
      __$WeatherCityDtoCopyWithImpl<$Res>;
  @override
  $Res call({@JsonKey(name: 'name') String name});
}

/// @nodoc
class __$WeatherCityDtoCopyWithImpl<$Res>
    extends _$WeatherCityDtoCopyWithImpl<$Res>
    implements _$WeatherCityDtoCopyWith<$Res> {
  __$WeatherCityDtoCopyWithImpl(
      _WeatherCityDto _value, $Res Function(_WeatherCityDto) _then)
      : super(_value, (v) => _then(v as _WeatherCityDto));

  @override
  _WeatherCityDto get _value => super._value as _WeatherCityDto;

  @override
  $Res call({
    Object name = freezed,
  }) {
    return _then(_WeatherCityDto(
      name: name == freezed ? _value.name : name as String,
    ));
  }
}

@JsonSerializable(
    fieldRename: FieldRename.snake, checked: true, explicitToJson: true)

/// @nodoc
class _$_WeatherCityDto implements _WeatherCityDto {
  const _$_WeatherCityDto({@JsonKey(name: 'name') this.name});

  factory _$_WeatherCityDto.fromJson(Map<String, dynamic> json) =>
      _$_$_WeatherCityDtoFromJson(json);

  @override
  @JsonKey(name: 'name')
  final String name;

  @override
  String toString() {
    return 'WeatherCityDto(name: $name)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _WeatherCityDto &&
            (identical(other.name, name) ||
                const DeepCollectionEquality().equals(other.name, name)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(name);

  @JsonKey(ignore: true)
  @override
  _$WeatherCityDtoCopyWith<_WeatherCityDto> get copyWith =>
      __$WeatherCityDtoCopyWithImpl<_WeatherCityDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_WeatherCityDtoToJson(this);
  }
}

abstract class _WeatherCityDto implements WeatherCityDto {
  const factory _WeatherCityDto({@JsonKey(name: 'name') String name}) =
      _$_WeatherCityDto;

  factory _WeatherCityDto.fromJson(Map<String, dynamic> json) =
      _$_WeatherCityDto.fromJson;

  @override
  @JsonKey(name: 'name')
  String get name;
  @override
  @JsonKey(ignore: true)
  _$WeatherCityDtoCopyWith<_WeatherCityDto> get copyWith;
}
