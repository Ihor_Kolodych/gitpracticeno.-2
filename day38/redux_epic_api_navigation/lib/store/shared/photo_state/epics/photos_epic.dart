import 'package:redux_epic_api_navigation/dto/photo_dto.dart';
import 'package:redux_epic_api_navigation/repsitories/photo_repository.dart';
import 'package:redux_epic_api_navigation/store/application/app_state.dart';
import 'package:redux_epic_api_navigation/store/shared/photo_state/photos_actions.dart';
import 'package:redux_epics/redux_epics.dart';
import 'package:rxdart/rxdart.dart';

class PhotoEpics {
  static const String tag = '[AuthEpics]';

  static final indexEpic = combineEpics<AppState>([
    getPhotoEpic,
  ]);

  static Stream<dynamic> getPhotoEpic(Stream<dynamic> actions, EpicStore<AppState> store) {
    return actions.whereType<GetPhotosAction>().switchMap((GetPhotosAction action) async* {
      final List<PhotoDto> a = await PhotoRepository.instance.getPhotos();
      yield SetPhotosToStateAction(a);
    });
  }
}
