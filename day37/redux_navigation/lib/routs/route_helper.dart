// import 'package:flutter/material.dart';
// import 'package:redux_navigation/main.dart';
// import 'package:redux_navigation/pages/page1.dart';
// import 'package:redux_navigation/pages/page2.dart';
// import 'package:redux_navigation/pages/page3.dart';
// import 'package:redux_navigation/pages/page4.dart';
// import 'package:redux_navigation/routs/routs.dart';
//
// class RouteHelper {
//   static const String TAG = '';
//   RouteHelper._privateConstructor();
//
//   static final RouteHelper _instance = RouteHelper._privateConstructor();
//
//   static RouteHelper get instance => _instance;
//
//   Route<dynamic> onGenerateRoute(RouteSettings settings) {
//     switch (settings.name) {
//       case (Routes.mainPage): return _createRoute(MyHomePage(),RouteSettings(name: Routes.mainPage));
//       case (Routes.page1): return _createRoute(Page1(),RouteSettings(name: Routes.page1));
//       case (Routes.page2): return _createRoute(Page2(),RouteSettings(name: Routes.page2));
//       case (Routes.page3): return _createRoute(Page3(),RouteSettings(name: Routes.page3));
//       case (Routes.page4): return _createRoute(Page4(),RouteSettings(name: Routes.page4));
//       default: return _createRoute(MyHomePage(),RouteSettings(name: Routes.mainPage));
//     }
//
//
//   }
//   PageRouteBuilder _createRoute(page,RouteSettings settings) {
//     return PageRouteBuilder(
//       settings: settings,
//       pageBuilder: (context, animation, secondaryAnimation) => page,
//     );
//   }
// }


import 'package:flutter/material.dart';
import 'package:redux_navigation/main.dart';
import 'package:redux_navigation/pages/page1.dart';
import 'package:redux_navigation/pages/page2.dart';
import 'package:redux_navigation/pages/page3.dart';
import 'package:redux_navigation/pages/page4.dart';
import 'package:redux_navigation/routs/routs.dart';

class RouteHelper {
  // region [Initialize]
  static const String TAG = '[RouteHelper]';
  RouteHelper._privateConstructor();
  static final RouteHelper _instance = RouteHelper._privateConstructor();
  static RouteHelper get instance => _instance;
  // endregion
  Route<dynamic> onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case Routes.mainPage:
        return _defaultRoute(
          settings: settings,
          page: MyHomePage(),
        );
      case Routes.page1:
        return _defaultRoute(
          settings: settings,
          page: Page1(),
        );
      case Routes.page2:
        return _defaultRoute(
          settings: settings,
          page: Page2(),
        );
      case Routes.page3:
        return _defaultRoute(
          settings: settings,
          page: Page3(),
        );
      case Routes.page4:
        return _defaultRoute(
          settings: settings,
          page: Page4(),
        );
      default:
        return _defaultRoute(
          settings: settings,
          page: MyHomePage(),
        );
    }
  }
  static PageRoute _defaultRoute({@required RouteSettings settings, @required Widget page}) {
    return MaterialPageRoute(
      settings: settings,
      builder: (BuildContext context) => page,
    );
  }
}