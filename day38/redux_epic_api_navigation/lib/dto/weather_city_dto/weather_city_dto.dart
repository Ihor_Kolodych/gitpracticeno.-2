import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:redux_epic_api_navigation/dto/weather_detail_dto/weather_detail_dto.dart';
import 'package:redux_epic_api_navigation/dto/weather_main_dto/weather_main_dto.dart';

part 'weather_city_dto.freezed.dart';

part 'weather_city_dto.g.dart';

@freezed
abstract class WeatherCityDto with _$WeatherCityDto {
  @JsonSerializable(
    fieldRename: FieldRename.snake,
    checked: true,
    explicitToJson: true,
  )
  const factory WeatherCityDto({
    @JsonKey(name: 'name') String name,

  }) = _WeatherCityDto;

  factory WeatherCityDto.fromJson(Map<String, dynamic> json) => _$WeatherCityDtoFromJson(json);
}