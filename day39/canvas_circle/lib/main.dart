import 'package:flutter/material.dart';

import 'circle/CirclePage.dart';
import 'const/size_const.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Canvas',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}
class _HomeState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    SizeUtil.getInstance().logicSize = MediaQuery.of(context).size;
    SizeUtil.initDesignSize();
    return  CirclePage();
  }
}
