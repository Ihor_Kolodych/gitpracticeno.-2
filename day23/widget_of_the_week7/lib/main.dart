import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:widget_of_the_week7/custom_tab.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  double _currentSliderValue = 0;
  double _currentSliderCupertinoValue = 0;
  RangeValues _currentRangeValues = const RangeValues(0, 100);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: DefaultTabController(
          length: 5,
          child: Scaffold(
            appBar: AppBar(
              bottom: TabBar(
                tabs: [
                  CustomTab(
                    'RichText',
                  ),
                  CustomTab(
                    'Slider',
                  ),
                  CustomTab(
                    'Range slider',
                  ),
                  CustomTab(
                    'Cupertion slider',
                  ),
                  CustomTab(
                    'Builder',
                  ),
                ],
              ),
            ),
            body: TabBarView(
              dragStartBehavior: DragStartBehavior.start,
              children: [
                RichText(
                  text: TextSpan(
                    style: DefaultTextStyle.of(context).style,
                    children: <TextSpan>[
                      TextSpan(text: 'Flutter ', style: TextStyle(color: Colors.yellow)),
                      TextSpan(text: 'Widget ', style: TextStyle(color: Colors.blue)),
                      TextSpan(text: 'of ', style: TextStyle(color: Colors.green)),
                      TextSpan(text: 'the ', style: TextStyle(color: Colors.grey)),
                      TextSpan(text: 'Week ', style: TextStyle(color: Colors.red)),
                    ],
                  ),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text('${(_currentSliderValue.round().toString())}'),
                    Slider(
                      value: _currentSliderValue,
                      min: 0,
                      max: 100,
                      label: _currentSliderValue.round().toString(),
                      onChanged: (double value) {
                        setState(() {
                          _currentSliderValue = value;
                        });
                      },
                    ),
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text('Left dot = ${(_currentRangeValues.start.round().toString())} Right dot = ${_currentRangeValues.end.round().toString()}'),
                    RangeSlider(
                      values: _currentRangeValues,
                      min: 0,
                      max: 100,
                      onChanged: (RangeValues values) {
                        setState(() {
                          _currentRangeValues = values;
                        });
                      },
                    ),
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text('${(_currentSliderCupertinoValue.round().toString())}'),
                    CupertinoSlider(
                      value: _currentSliderCupertinoValue,
                      min: 0,
                      max: 100,
                      onChanged: (double value) {
                        setState(() {
                          _currentSliderCupertinoValue = value;
                        });
                      },
                    ),
                  ],
                ),
                Container(child: Builder(
                  builder: (BuildContext context) {
                    return InkWell(
                      child: Center(
                        child: Text('Tap me, i\'am Builder'),
                      ),
                      onTap: () {
                        Scaffold.of(context).showSnackBar(
                          SnackBar(
                            content: Text('From Scaffold.of(context)'),
                          ),
                        );
                      },
                    );
                  },
                )),
              ],
            ),
          ),
        ));
  }
}
