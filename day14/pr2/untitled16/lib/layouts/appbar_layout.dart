import 'package:flutter/material.dart';

import 'drawer_layout.dart';

class AppBarLayout extends PreferredSize {
  @override
  Size get preferredSize => Size.fromHeight(200);
  final GlobalKey _scaffoldKey = new GlobalKey();
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        child: Column(
          children: [
            Divider(
              height: 2.0,
              thickness: 2.5,
              color: Colors.black,
            ),
            Container(
              padding: EdgeInsets.only(top: 12.0, bottom: 15.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    height: 20.0,
                    width: 20.0,
                    margin: EdgeInsets.only(left: 20),
                    child: InkWell(
                      onTap: (){Scaffold.of(context).openDrawer();},
                      child: (Column(
                        children: [
                          Divider(
                            height: 5.0,
                            color: Colors.black,
                            thickness: 2.0,
                          ),
                          Divider(
                            height: 5.0,
                            color: Colors.black,
                            indent: 10.0,
                            thickness: 2.0,
                          ),
                          SizedBox(),
                        ],
                      )),
                    ),
                  )
                ],
              ),
            ),
            Divider(
              height: 25.0,
              thickness: 2.5,
              color: Colors.black,
            ),
          ],
        ),
      ),
    );
  }
}
