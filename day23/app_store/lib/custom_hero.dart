import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomHero extends StatelessWidget {
  final String imageBackGround;
  final String imageIcon;
  final String textGameOfTheDay;
  final String textNameGame;
  final String textGenre;
  final double price;
  const CustomHero({this.imageBackGround, this.imageIcon, this.textGameOfTheDay, this.textNameGame, this.textGenre, this.price});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Hero(
        tag: 'asd',
        child: SingleChildScrollView(
          child: GestureDetector(
            onTap: (){
              Navigator.pop(context);
            },
            child: Column(
              children: [
                Container(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage(imageBackGround),
                      fit: BoxFit.cover,
                    ),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(right: 20.0),
                        child: Container(
                          width: 60,
                          child: Directionality(
                            textDirection: TextDirection.rtl,
                            child: Text(
                              textGameOfTheDay,
                              style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15),
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 10, bottom: 10, left: 20, right: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            ClipRRect(
                              borderRadius: BorderRadius.circular(10.0),
                              child: Container(
                                  color: Colors.white,
                                  child: Padding(
                                    padding: const EdgeInsets.only(top: 3.0, bottom: 3.0, right: 8, left: 8),
                                    child: Text(
                                      '\$${price}',
                                      style: TextStyle(color: Colors.blue, fontSize: 12.0, fontWeight: FontWeight.bold),
                                    ),
                                  )),
                            ),
                            Row(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(right: 15),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    children: [
                                      Container(
                                        child: Text(
                                          textNameGame,
                                          style: TextStyle(color: Colors.white, fontSize: 10.0),
                                        ),
                                      ),
                                      Container(
                                        child: Text(
                                          textGenre,
                                          style: TextStyle(color: Colors.white, fontSize: 10.0),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                ClipRRect(
                                  borderRadius: BorderRadius.circular(8.0),
                                  child: Container(
                                    decoration: BoxDecoration(
                                        image: DecorationImage(
                                          fit: BoxFit.fitWidth,
                                          alignment: FractionalOffset.topCenter,
                                          image: ExactAssetImage(imageIcon),
                                        )),
                                    height: 40.0,
                                    width: 40.0,
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      )
                    ],
                  ), /* add child content here */
                ),
                Material(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(child: Text('ewkflg;kwel;fkewl;fkewl;fkewl;'
                        'fkwekfewkfewfjkhbewqjkdhewqjfhweqjhkfhweqjfhgewhjfhewjfhewjhfjewhfjewhfjewhfjewhfjhejkfhwejkfhwejkfhejwkhfjkwehfewhjkfhwejkfhejwkh'
                        'fjwekhfjkewhfjkewhjfkhewjfhewjkfhwejkfhwejkfhwejkfhwejkfhwejkfhjkwehfjkewhfjkewfjhewjfkhewjfkhwejkfhewjkfhwejkfhwefjkewhfjkewhfjke'
                        'whfjkewhfjhewfjkwehfjkewhfjkwehfjkwehjkfhewjkfhwejfwekjhfewjkfhewjkfhwefjkwefwjkehfjwkejjwefhweqfjsdhfjkwefwqwaedysdafcvrewvfkljef'
                        'sdcxvywefwleikfjhsddvjiwoefsdkjfklwjekf'
                        ''),),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
