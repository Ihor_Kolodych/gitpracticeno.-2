import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:redux_navigation/store/application/app_state.dart';
import 'package:redux_navigation/store/shared/settings_page_state/settings_page_action.dart';



class SettingsPageSelectors {

  static num getCounterValue(Store<AppState> store) {
    return store.state.settingsPageState.counter;
  }

  static num getDivideValueTwo(Store<AppState> store) {
    return store.state.settingsPageState.divideCounter;
  }

  static num getMultipleValueTen(Store<AppState> store) {
    return store.state.settingsPageState.multipleCounter;
  }

  static void Function() getIncrementCounterFunction(Store<AppState> store) {
    return () => store.dispatch(IncrementAction());
  }

  static void Function() getDecrementCounterFunction(Store<AppState> store) {
    return () => store.dispatch(DecrementAction());
  }

  static void Function(Color) getChangeColorBackground(Store<AppState> store) {
    return (Color color) => store.dispatch(ChangeColorBackgroundAction(color: color));
  }

  static Color getColorBackground(Store<AppState> store) {
    return store.state.settingsPageState.color;
  }

}