import 'package:flutter/material.dart';
import '../models/listofpeople.dart';
import 'package:intl/intl.dart';

class PeopleList extends StatelessWidget {
  final List<People> peoples;

  PeopleList(this.peoples);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemBuilder: (ctx, index) {
        return Card(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                margin: EdgeInsets.symmetric(
                  vertical: 15,
                  horizontal: 15,
                ),
                padding: EdgeInsets.all(10),
                child: Text(
                  peoples[index].id,
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text(
                    peoples[index].name,
                  ),
                  Text(
                    DateFormat.yMMMd().format(peoples[index].date),
                    style: TextStyle(
                      color: Colors.grey,
                      fontSize: 16,
                    ),
                  ),
                ],
              ),
            ],
          ),
        );
      },
      itemCount: peoples.length,
    );
  }
}
