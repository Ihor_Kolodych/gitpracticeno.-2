import 'dart:collection';

import 'package:api_redux_weather/dto/openweather_dto.dart';
import 'package:api_redux_weather/store/app/reducer.dart';
import 'package:api_redux_weather/store/pages/photos_state/photos_action.dart';

class WeatherState {
  final OpenWeatherDto weathers;
  final OpenWeatherDto selectedWeather;
  final String selectedId;

  WeatherState(
    this.weathers,
    this.selectedId,
    this.selectedWeather,
  );

  factory WeatherState.initial() => WeatherState(
      OpenWeatherDto(
        id: "800",
        temp: "200",
        name: "www",
        country: "Country",
        descriptionWeather: "descriptionWeather",
        wind: "222",
      ),
      '',
      OpenWeatherDto(id: "800",
        temp: "200",
        name: "www",
        country: "Country",
        descriptionWeather: "descriptionWeather",
        wind: "222",));

  WeatherState copyWith({
    OpenWeatherDto weathers,
    String id,
    OpenWeatherDto selectedWeather,
  }) {
    return WeatherState(
      weathers ?? this.weathers,
      id ?? this.selectedId,
      selectedWeather ?? this.selectedWeather,
    );
  }

  WeatherState reducer(dynamic action) {
    return Reducer<WeatherState>(
      actions: HashMap.from({
        SaveImagesAction: (dynamic action) => saveImages((action as SaveImagesAction).weathers),
      }),
    ).updateState(action, this);
  }

  WeatherState saveImages(OpenWeatherDto weathers) {
    return copyWith(weathers: weathers, selectedWeather: weathers);
  }
}
