import 'package:flutter/material.dart';

class ChangeColorBackgroundAction {
  Color color;

  ChangeColorBackgroundAction({
    @required this.color,
  });
}

class IncrementAction {
  IncrementAction();
}

class DecrementAction {
  DecrementAction();
}
