import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:state_managment_practice/store/shared/reducer.dart';
import 'package:state_managment_practice/store/shared/settings_page_state/settings_page_action.dart';

class SettingsPageState {
  final Color color;

  SettingsPageState({
    this.color,
  });

  factory SettingsPageState.initial() {
    return SettingsPageState(
      color: Colors.white,
    );
  }

  SettingsPageState copyWith({
    Color color,
  }) {
    return SettingsPageState(
      color: color ?? this.color,
    );
  }

  SettingsPageState reducer(dynamic action) {
    return Reducer<SettingsPageState>(
      actions: HashMap.from({
        ChangeColorBackgroundAction: (dynamic action) => _changeColorBackground(action as ChangeColorBackgroundAction),
      }),
    ).updateState(action, this);
  }

  SettingsPageState _changeColorBackground(ChangeColorBackgroundAction action){
    return copyWith(color: action.color);
  }
}