import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../providers/provider_text.dart';

class SomeButtons extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    TextProvider textProvider = Provider.of<TextProvider>(context, listen: false);
    return Container(
      child: Center(
        child: Column(
          children: [
            ...textProvider.listOfStrings.map((value) {
              return Container(
                margin: EdgeInsets.all(20),
                alignment: Alignment.center,
                padding: EdgeInsets.all(20),
                color: Colors.blue,
                child: InkWell(
                  onTap: () {
                    textProvider.changeText(value);
                  },
                  child: Text(
                    value,
                    style: TextStyle(fontSize: 30),
                  ),
                ),
              );
            })
          ],
        ),
      ),
    );
  }
}
