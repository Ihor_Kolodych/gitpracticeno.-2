import 'package:api_redux_weather/dto/openweather_dto.dart';
import 'package:api_redux_weather/store/app/app_state.dart';
import 'package:api_redux_weather/store/pages/photos_state/photos_action.dart';
import 'package:redux/redux.dart';
class PhotosSelector {


  static void Function(String id) selectImage(Store <AppState> store) {
    return (String id) => store.dispatch(SelectImageAction(id: id));
  }

  static void Function() getImagesFunction(Store<AppState> store) {
    return () => store.dispatch(GetImagesAction());
  }


  static String getSelectedImageId(Store<AppState> store) {
    return store.state.weatherState.selectedId;
  }

  static OpenWeatherDto getSelectedImage(Store<AppState> store) {
    return store.state.weatherState.selectedWeather;
  }


}