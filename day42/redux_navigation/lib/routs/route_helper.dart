import 'package:flutter/material.dart';
import 'package:redux_navigation/main.dart';
import 'package:redux_navigation/pages/page1.dart';
import 'package:redux_navigation/pages/page2.dart';
import 'package:redux_navigation/pages/page3.dart';
import 'package:redux_navigation/routs/routs.dart';

class RouteHelper {
  // region [Initialize]
  static const String TAG = '[RouteHelper]';
  RouteHelper._privateConstructor();
  static final RouteHelper _instance = RouteHelper._privateConstructor();
  static RouteHelper get instance => _instance;
  // endregion
  Route<dynamic> onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case Routes.mainPage:
        return _defaultRoute(
          settings: settings,
          page: MyHomePage(),
        );
      case Routes.page1:
        return _defaultRoute(
          settings: settings,
          page: Page1(),
        );
      case Routes.page2:
        return _defaultRoute(
          settings: settings,
          page: Page2(),
        );
      case Routes.page3:
        return _defaultRoute(
          settings: settings,
          page: Page3(),
        );
      default:
        return _defaultRoute(
          settings: settings,
          page: MyHomePage(),
        );
    }
  }
  static PageRoute _defaultRoute({@required RouteSettings settings, @required Widget page}) {
    return MaterialPageRoute(
      settings: settings,
      builder: (BuildContext context) => page,
    );
  }
}