import 'dart:collection';

import 'package:api_redux_city/dto/city_dto.dart';
import 'package:api_redux_city/store/app/reducer.dart';
import 'package:api_redux_city/store/pages/photos_state/city_action.dart';

class CityState {
  final List<CityDto> citys;
  final CityDto selectedCity;
  final String selectedId;

  CityState(
    this.citys,
    this.selectedId,
    this.selectedCity,
  );

  factory CityState.initial() => CityState([], '', CityDto());

  CityState copyWith({
    List<CityDto> citys,
    String id,
    CityDto selectedCitys,
  }) {
    return CityState(
      citys ?? this.citys,
      id ?? this.selectedId,
      selectedCitys ?? this.selectedCity,
    );
  }

  CityState reducer(dynamic action) {
    return Reducer<CityState>(
      actions: HashMap.from({
        SaveCityAction: (dynamic action) => saveCitys((action as SaveCityAction).citys),
        SelectCitysAction: (dynamic action) => selectCitys((action as SelectCitysAction).id)
      }),
    ).updateState(action, this);
  }

  CityState saveCitys(List<CityDto> weathers) {
    return copyWith(citys: weathers);
  }
  CityState selectCitys(String id) {
    CityDto selectedCity = citys.firstWhere((selectedCity) => selectedCity.id == id);
    return copyWith(id: id, selectedCitys: selectedCity);
  }
}
