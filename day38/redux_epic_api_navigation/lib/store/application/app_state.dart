import 'package:flutter/cupertino.dart';
import 'package:redux_epic_api_navigation/store/shared/photo_state/epics/photos_epic.dart';
import 'package:redux_epic_api_navigation/store/shared/photo_state/photos_state.dart';
import 'package:redux_epic_api_navigation/store/shared/weather_state/epics/weather_epic.dart';
import 'package:redux_epic_api_navigation/store/shared/weather_state/weather_state.dart';
import 'package:redux_epics/redux_epics.dart';

/// Class [AppState], is the main [state] application.
/// It keeps 3, smaller states.
/// Namely, [dialogState], [storageState], [loaderState].
/// First [dialogState], this variable stores the state of dialogs, it is used to call various dialogs.
/// Second [storageState], the primary state, stores all information from all states.
/// The third [loaderState] is required to loading.
class AppState {
  final PhotoState photosState;
  final WeatherState weatherState;

  AppState({
    @required this.photosState,
    @required this.weatherState,
  });

  ///All states are initialized in the [initial] function.
  factory AppState.initial() {
    return AppState(
      photosState: PhotoState.initial(),
      weatherState: WeatherState.initial(),
    );
  }

  ///The [getReducer] function is the main Reducer in which you call Reducer, all other states.
  static AppState getReducer(AppState state, dynamic action) {
    return AppState(
      photosState: state.photosState.reducer(action),
      weatherState: state.weatherState.reducer(action),
    );
  }

  ///In [getAppEpic], call the main epic.
  static final getAppEpic = combineEpics<AppState>([
    PhotoEpics.indexEpic,
    WeatherEpics.indexEpic,
  ]);
}
