import 'package:flutter/material.dart';

class DateStore {
  final String dateTime;
  final String timeOfDay;
  final double currentSliderValue;
  final String valueDropdownButton;

  DateStore(this.dateTime, this.timeOfDay, this.currentSliderValue, this.valueDropdownButton);

  // String display(){
  //   return '$dateTime,$timeOfDay,$currentSliderValue,$valueDropdownButton';
  // }


  @override
  String toString() {
    return  'Date time: $dateTime, \n Time of day: $timeOfDay, \n Current slider val: $currentSliderValue, \n Gender: $valueDropdownButton';
  }
}