import 'package:flutter/foundation.dart';

class PhotoDto {
  final String author;
  final String url;

  PhotoDto({
    @required this.author,
    @required this.url,
  });

  factory PhotoDto.fromJson(Map<String, dynamic> json) {
    return PhotoDto(
      author: json["author"],
      url: json["download_url"],
    );
  }
}
