import 'package:flutter/material.dart';
import './models/listofpeople.dart';
import './widgets/people_list.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Task',
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final List<People> _peopleList = [
    People(
      id: 't1',
      name: 'New Short',
      date: DateTime.now(),
    ),
    People(
      id: 't2',
      name: 'New Man',
      date: DateTime.now(),
    ),
    People(
      id: 't3',
      name: 'New Man',
      date: DateTime.now(),
    ),
    People(
      id: 't4',
      name: 'New Man',
      date: DateTime.now(),
    ),
    People(
      id: 't5',
      name: 'New Man',
      date: DateTime.now(),
    ),
    People(
      id: 't6',
      name: 'New Man',
      date: DateTime.now(),
    ),
    People(
      id: 't7',
      name: 'New Man',
      date: DateTime.now(),
    ),
    People(
      id: 't7',
      name: 'New Man',
      date: DateTime.now(),
    ),People(
      id: 't7',
      name: 'New Man',
      date: DateTime.now(),
    ),

  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body:  PeopleList(_peopleList),
    );
  }
}
