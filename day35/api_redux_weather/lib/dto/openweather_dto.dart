import 'package:flutter/foundation.dart';

class OpenWeatherDto {
  final String id;
  final String descriptionWeather;
  final String wind;
  final String country;
  final String temp;
  final String name;

  OpenWeatherDto({
    @required this.id,
    @required this.descriptionWeather,
    @required this.wind,
    @required this.country,
    @required this.temp,
    @required this.name,
  });

  factory OpenWeatherDto.fromJson(Map<String, dynamic> json) {
    return OpenWeatherDto(
      id: json["weather"][0]["id"].toString(),
      descriptionWeather: json["weather"][0]["description"].toString(),
      wind: json["wind"]["speed"].toString(),
      country: json["sys"]["country"].toString(),
      temp: json["main"]["temp"].toString(),
      name: json["name"].toString(),
    );
  }
}
