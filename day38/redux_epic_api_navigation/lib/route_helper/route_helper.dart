import 'package:flutter/material.dart';
import 'package:redux_epic_api_navigation/route_helper/routes.dart';
import 'package:redux_epic_api_navigation/ui/photo_page/photo_page.dart';
import 'package:redux_epic_api_navigation/ui/weather_page/weather_page.dart';

class RouteHelper {
  static const String TAG = '';
  RouteHelper._privateConstructor();

  static final RouteHelper _instance = RouteHelper._privateConstructor();

  static RouteHelper get instance => _instance;

  Route<dynamic> onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case (Routes.photoPage): return _createRoute(PhotoPage(),RouteSettings(name: Routes.photoPage));
      case (Routes.weatherPage): return _createRoute(WeatherPage(),RouteSettings(name: Routes.weatherPage));
      default:
    }


  }
  PageRouteBuilder _createRoute(page,RouteSettings settings) {
    return PageRouteBuilder(
        settings: settings,
        pageBuilder: (context, animation, secondaryAnimation) => page,
    );
  }
}
