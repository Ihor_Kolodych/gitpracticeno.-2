import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:redux_epic_api_navigation/route_helper/routes.dart';

class RouteService {
  static const tag = '[RouteService]';

  RouteService._privateConstructor();

  static final RouteService _instance = RouteService._privateConstructor();

  static RouteService get instance => _instance;

  List<String> _routesStack = [];

  bool canPop() {
    if (_routesStack.length <= 1) {
      return false;
    }
    return true;
  }

  NavigateToAction push(String route_name) {
    _routesStack.removeWhere((route) => route == route_name);
    _routesStack.add(route_name);
    print(_routesStack);
    return NavigateToAction.replace(route_name);
  }

  NavigateToAction pushReplacedNamed(String route_name) {
    return NavigateToAction.replace(route_name);
  }

}