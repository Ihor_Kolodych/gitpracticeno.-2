import 'package:api_redux_weather/dto/openweather_dto.dart';
import 'package:api_redux_weather/store/app/app_state.dart';
import 'package:api_redux_weather/store/pages/photos_state/photos_selector.dart';
import 'package:flutter/foundation.dart';
import 'package:redux/redux.dart';



class WeatherPageViewModel {
  final OpenWeatherDto weather;
  final void Function() getWeather;

  WeatherPageViewModel({
    @required this.weather,
    @required this.getWeather,
  });

  static WeatherPageViewModel fromStore(Store<AppState> store) {
    return WeatherPageViewModel(
      weather: PhotosSelector.getSelectedImage(store),
      getWeather: PhotosSelector.getImagesFunction(store),
    );
  }
}
