import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux_epic_api_navigation/store/application/app_state.dart';

import 'weather_page_vm.dart';
import 'weather_element.dart';

class WeatherListWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, WeatherPageVM>(
      converter: WeatherPageVM.fromStore,
      onInitialBuild: (vm) => vm.getWeather(),
      builder: (BuildContext context, WeatherPageVM vm) {
        if (vm.weatherList != null) {
          return Scaffold(
            appBar: AppBar(),
                body: Container(
                  width: double.infinity,
                  height: MediaQuery.of(context).size.height,
                  child: SingleChildScrollView(
                    physics: ScrollPhysics(),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text('${vm.weatherList.city.name}'),
                        //Text('${vm.weatherList.list.length}'),
                        SingleChildScrollView(
                          child: ListView.builder(
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                              itemCount: vm.weatherList.list.length,
                              itemBuilder: (BuildContext ctx, int index) {
                                return Center(
                                  child: Text(
                                      'Day: $index' ' temp:${vm.weatherList.list[index].main.temp}'),
                                );
                              }),
                        )
                      ],
                    ),
                  ),
                ),
                // body: ListView(
                //   itemExtent: 144.0,
                //   children: [
                //     ...vm.weatherList.map(
                //       (weather) {
                //         return WeatherElement(
                //           weather: weather,
                //         );
                //       },
                //     ).toList(),
                //   ],
                // ),
                floatingActionButton: FloatingActionButton(
                  onPressed: () => vm.goToPhoto(),
                ),
              );
        } else {
          return Material(
                child: Center(
                  child: Container(
                    child: Text('Empty list'),
                  ),
                ),
              );
        }
      },
    );
  }
}
