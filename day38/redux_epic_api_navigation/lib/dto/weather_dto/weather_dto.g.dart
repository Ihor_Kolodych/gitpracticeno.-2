// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'weather_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_WeatherDto _$_$_WeatherDtoFromJson(Map<String, dynamic> json) {
  return $checkedNew(r'_$_WeatherDto', json, () {
    final val = _$_WeatherDto(
      cod: $checkedConvert(json, 'cod', (v) => v as String),
      list: $checkedConvert(
          json,
          'list',
          (v) => (v as List)
              ?.map((e) => e == null
                  ? null
                  : WeatherDetailDto.fromJson(e as Map<String, dynamic>))
              ?.toList()),
      city: $checkedConvert(
          json,
          'city',
          (v) => v == null
              ? null
              : WeatherCityDto.fromJson(v as Map<String, dynamic>)),
    );
    return val;
  });
}

Map<String, dynamic> _$_$_WeatherDtoToJson(_$_WeatherDto instance) =>
    <String, dynamic>{
      'cod': instance.cod,
      'list': instance.list?.map((e) => e?.toJson())?.toList(),
      'city': instance.city?.toJson(),
    };
