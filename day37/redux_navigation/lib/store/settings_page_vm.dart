import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:redux/redux.dart';
import 'package:redux_navigation/routs/route_selectors.dart';
import 'package:redux_navigation/routs/routs.dart';
import 'package:redux_navigation/store/shared/settings_page_state/settings_page_selectors.dart';


import 'application/app_state.dart';

class SettingsPageViewModel {
  Color color;
  final void Function(Color color) changeColorBackground;
  final void Function(String route) gotoMainPage;

  SettingsPageViewModel({
    @required this.color,
    @required this.changeColorBackground,
    @required this.gotoMainPage,
  });

  static SettingsPageViewModel fromStore(Store<AppState> store) {
    return SettingsPageViewModel(
      changeColorBackground: SettingsPageSelectors.getChangeColorBackground(store),
      color: SettingsPageSelectors.getColorBackground(store),
      gotoMainPage: RouteSelectors.gotoPage(store),
    );
  }
}
