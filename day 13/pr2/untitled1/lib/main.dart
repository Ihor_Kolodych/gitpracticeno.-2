import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:untitled1/pages/second_page.dart';
import './providers/counter_provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (ctx) => TextProvider(),
      child: MaterialApp(
        home: MyHomePage(),
        initialRoute: '/',
        routes: {
          '/second': (context) => SecondPage(),
        },
      ),
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    TextProvider textProvider = Provider.of<TextProvider>(context);
    return Scaffold(
      appBar: AppBar(
        leading: InkWell(
          child: Icon(Icons.arrow_forward),
          onTap: (() => Navigator.pushNamed(context, '/second')),
        ),
      ),
      body: Center(
        child: Container(
          child: Text(
            '${textProvider.Sum}',
            style: TextStyle(fontSize: 40),
          ),
        ),
      ),
    );
  }
}
