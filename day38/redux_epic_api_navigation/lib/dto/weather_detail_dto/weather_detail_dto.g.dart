// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'weather_detail_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_WeatherDetailDto _$_$_WeatherDetailDtoFromJson(Map<String, dynamic> json) {
  return $checkedNew(r'_$_WeatherDetailDto', json, () {
    final val = _$_WeatherDetailDto(
      dt: $checkedConvert(json, 'dt', (v) => v as num),
      main: $checkedConvert(
          json,
          'main',
          (v) => v == null
              ? null
              : WeatherMainDto.fromJson(v as Map<String, dynamic>)),
    );
    return val;
  });
}

Map<String, dynamic> _$_$_WeatherDetailDtoToJson(
        _$_WeatherDetailDto instance) =>
    <String, dynamic>{
      'dt': instance.dt,
      'main': instance.main?.toJson(),
    };
