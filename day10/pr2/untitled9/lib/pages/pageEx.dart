import 'package:flutter/material.dart';

class SecondScreen extends StatelessWidget {
  static const _url = [
    {
      'color': Colors.red,
      'url': 'https://sun9-22.userapi.com/s/v1/if1/H7Xnl4D-VUT3dx1UqHOkz6-Bdvp4Uo-hwnR9V9Ax-UuqVOmHtpUjp3w-bzmXL7lH2ChaBjxC'
          '.jpg?size=200x0&quality=96&crop=0,0,960,960&ava=1'
    },
    {
      'color': Colors.blue,
      'url': 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ6ZFgy5BAvjhqGr6eWhHulrSs-geyMW8BLk8wURrbUPMSZ1DPZjGhbl0C4wUQg7GnZifc&usqp=CAU',
    },
    {
      'color': Colors.purple,
      'url': 'https://encrypted-tbn0.gstatic'
          '.com/images?q=tbn:ANd9GcTPzEwiCdIHs9Xly7af8SyHRDGlCjTtV7HLKCL9SbZxK3PQ4EocuKSPJe0QRN8BkqDGFPE&usqp=CAU',
    },{
      'color': Colors.red,
      'url': 'https://sun9-22.userapi.com/s/v1/if1/H7Xnl4D-VUT3dx1UqHOkz6-Bdvp4Uo-hwnR9V9Ax-UuqVOmHtpUjp3w-bzmXL7lH2ChaBjxC'
          '.jpg?size=200x0&quality=96&crop=0,0,960,960&ava=1'
    },
    {
      'color': Colors.blue,
      'url': 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ6ZFgy5BAvjhqGr6eWhHulrSs-geyMW8BLk8wURrbUPMSZ1DPZjGhbl0C4wUQg7GnZifc&usqp=CAU',
    },
    {
      'color': Colors.purple,
      'url': 'https://encrypted-tbn0.gstatic'
          '.com/images?q=tbn:ANd9GcTPzEwiCdIHs9Xly7af8SyHRDGlCjTtV7HLKCL9SbZxK3PQ4EocuKSPJe0QRN8BkqDGFPE&usqp=CAU',
    }
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Second Screen'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Flexible(
            flex: 90,
            child: ListView.builder(
              scrollDirection: Axis.vertical,
              itemBuilder: (ctx, index) {
                return Center(
                  child: Container(
                    height: 100,
                    width: 100,
                    margin: EdgeInsets.all(20.0),
                    color: _url[index]['color'],
                    child: ClipOval(child: Image.network(_url[index]['url'])),
                  ),
                );
              },
              itemCount: _url.length,
            ),
          ),
          Flexible(
            flex: 10,
            child: Material(
              child: ElevatedButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text(
                  'Last page',
                  style: TextStyle(fontSize: 20),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
