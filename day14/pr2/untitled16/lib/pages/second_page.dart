import 'package:flutter/material.dart';
import 'package:untitled16/layouts/appbar_layout.dart';
import 'package:untitled16/layouts/drawer_layout.dart';

class SecondPage extends StatelessWidget {
  final String argument;

  const SecondPage(this.argument);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawerEnableOpenDragGesture: false,
      backgroundColor: Colors.brown,
      body: Text('Second Page'),
      appBar: AppBarLayout(),
      drawer: DrawerLayout(),
    );
  }
}
