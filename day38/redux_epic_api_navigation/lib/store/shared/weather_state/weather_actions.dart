import 'package:redux_epic_api_navigation/dto/weather_dto/weather_dto.dart';
import 'package:redux_epic_api_navigation/store/shared/base_action.dart';

class GetWeatherAction extends BaseAction {

}

class SetWeatherToStateAction extends BaseAction {
  final WeatherDto weather;

  SetWeatherToStateAction(this.weather);
}