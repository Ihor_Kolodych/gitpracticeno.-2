
import 'package:api_redux_weather/dto/openweather_dto.dart';
import 'package:api_redux_weather/repsitories/weather_repository.dart';
import 'package:api_redux_weather/store/app/app_state.dart';
import 'package:api_redux_weather/store/pages/photos_state/photos_action.dart';
import 'package:redux_epics/redux_epics.dart';
import 'package:rxdart/rxdart.dart';

import '../../shared/empty_action.dart';

class WeatherEpics {
  static final indexEpic = combineEpics<AppState>([
    getImagesEpic,
    setImagesEpic,
  ]);

  static Stream<dynamic> getImagesEpic(Stream<dynamic> actions, EpicStore<AppState> store) {
    return actions.whereType<GetImagesAction>().switchMap(
      (action) {
         // List<ImageDto> images = await UnsplashImageRepository.instance.getImages();
         // if(images!= null && images.isNotEmpty){
         //   Stream.value(SaveImagesAction(images: images));
         // }
        return Stream.fromFuture(
          WeatherRepository.instance.getImages().then(
            (OpenWeatherDto images) {
              if (images == null) {
                return EmptyAction();
              }
              return SaveImagesAction(
                weathers: images,
              );
            },
          ),
        );
      },
    );
  }

  static Stream<dynamic> setImagesEpic(Stream<dynamic> actions, EpicStore<AppState> store) {
    return actions.whereType<SaveImagesAction>().switchMap(
      (action) {
        return Stream.fromIterable(
          [
            EmptyAction(),
          ],
        );
      },
    );
  }

}
