import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:redux_epic_api_navigation/dto/weather_city_dto/weather_city_dto.dart';
import 'package:redux_epic_api_navigation/dto/weather_detail_dto/weather_detail_dto.dart';

part 'weather_dto.freezed.dart';

part 'weather_dto.g.dart';

@freezed
abstract class WeatherDto with _$WeatherDto {
  @JsonSerializable(
    fieldRename: FieldRename.snake,
    checked: true,
    explicitToJson: true,
  )
  const factory WeatherDto({
    @JsonKey(name: 'cod') String cod,
    @JsonKey(name: 'list') List<WeatherDetailDto> list,
    @JsonKey(name: 'city') WeatherCityDto city,

  }) = _WeatherDto;

  factory WeatherDto.fromJson(Map<String, dynamic> json) => _$WeatherDtoFromJson(json);
}