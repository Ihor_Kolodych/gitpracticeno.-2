import 'package:flutter/material.dart';
import 'package:redux_navigation/main.dart';

class Page1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MyHomePage(child: Center(child: Text('Page1')));
  }
}
