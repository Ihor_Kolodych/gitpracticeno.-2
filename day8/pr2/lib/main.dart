import 'dart:math';

import 'package:flutter/material.dart';
import 'package:untitled7/widgets/custom_container.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<Color> _availableColors = [
    Colors.red,
    Colors.blue,
    Colors.purple,
  ];
  List<String> url = [
    'https://sun9-22.userapi.com/s/v1/if1/H7Xnl4D-VUT3dx1UqHOkz6-Bdvp4Uo-hwnR9V9Ax-UuqVOmHtpUjp3w-bzmXL7lH2ChaBjxC'
        '.jpg?size=200x0&quality=96&crop=0,0,960,960&ava=1',
    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ6ZFgy5BAvjhqGr6eWhHulrSs-geyMW8BLk8wURrbUPMSZ1DPZjGhbl0C4wUQg7GnZifc&usqp=CAU',
    'https://encrypted-tbn0.gstatic'
        '.com/images?q=tbn:ANd9GcTPzEwiCdIHs9Xly7af8SyHRDGlCjTtV7HLKCL9SbZxK3PQ4EocuKSPJe0QRN8BkqDGFPE&usqp=CAU',
  ];

 @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        child: ListView.builder(
          itemBuilder: (ctx, index) {
            return CustomContainer(_availableColors[index], url[index]);
          },
          itemCount: _availableColors.length,
        ),
      ),
      appBar: AppBar(),
      body: ListView.builder(
        itemBuilder: (ctx, index) {
          return CustomContainer(_availableColors[index], url[index]);
        },
        itemCount: _availableColors.length,
      ),
    );
  }
}

