// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'weather_main_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_WeatherMainDto _$_$_WeatherMainDtoFromJson(Map<String, dynamic> json) {
  return $checkedNew(r'_$_WeatherMainDto', json, () {
    final val = _$_WeatherMainDto(
      temp: $checkedConvert(json, 'temp', (v) => v as num),
    );
    return val;
  });
}

Map<String, dynamic> _$_$_WeatherMainDtoToJson(_$_WeatherMainDto instance) =>
    <String, dynamic>{
      'temp': instance.temp,
    };
