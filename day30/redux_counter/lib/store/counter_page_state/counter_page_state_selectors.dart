import 'package:redux_counter/store/app/app_state.dart';
import 'package:redux/redux.dart';
import 'package:redux_counter/store/counter_page_state/counter_page_state_action.dart';

class CounterPageStateSelectors{
  static void Function() incrementCounterFunction(Store<AppState> store){
    return () => store.dispatch(IncrementAction());
  }

  static void Function() decrementCounterFunction(Store<AppState> store){
    return () => store.dispatch(DecrementAction());
  }

  static int counterShowFunction(Store<AppState> store){
    return store.state.counterPageStateState.counter;
  }
}
