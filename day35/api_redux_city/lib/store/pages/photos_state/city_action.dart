import 'package:api_redux_city/dto/city_dto.dart';

class GetCityAction {
  GetCityAction();
}

class SetInitCityAction {
  final List<CityDto> citys;

  SetInitCityAction({this.citys});
}

class SaveCityAction {
  List<CityDto> citys;

  SaveCityAction({this.citys});
}

class SelectCitysAction {
  String id;

  SelectCitysAction({this.id});
}
