import 'package:dictionary_practice/dictionary/dictionary_classes/genaral_language.dart';
import 'package:dictionary_practice/dictionary/dictionary_classes/setting_page_dictionary.dart';
import 'package:dictionary_practice/dictionary/models/language.dart';

const Language ru = Language(
  generalLanguage: GeneralLanguage(
    text: 'Тест приложение',
  ),
  settingsPageDictionary: SettingsPageDictionary(
    textTwo: 'текстдва',
  ),
);
