import 'package:flutter/material.dart';
import 'package:untitled7/pages/firs_page.dart';
import 'package:untitled7/pages/second_page.dart';
import 'package:untitled7/pages/error_page.dart';

import 'main.dart';

class RouteGenerater{
  static const ROUTE_FIRST = "/first";
  static const ROUTE_SECOND = "/second";
  static const ROUTE_MAIN= "/main";

  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case ROUTE_FIRST:
        final page = FirstPage(settings.arguments);
        return MaterialPageRoute(builder: (context) => page);
      case ROUTE_SECOND:
        final page = SecondScreen(settings.arguments);
        return MaterialPageRoute(builder: (context) => page);
      case ROUTE_MAIN:
        final page = HomePage(argument: settings.arguments);
        return MaterialPageRoute(builder: (context) => page);
      default:
        return MaterialPageRoute(
            builder: (context) => Error());
    }
  }
}