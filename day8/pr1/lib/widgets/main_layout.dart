import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class MainLayout extends StatelessWidget {
  final Widget child;
  final PreferredSizeWidget appBar;
  final EdgeInsets padding;
  final Color color;
  final EdgeInsets margin;

  const MainLayout({Key key, this.appBar, this.child, this.padding, this.color, this.margin}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar,
      body: Column(
        children: [
          Material(
            color: color,
            child: Container(
              child: child,
              padding: padding ?? const EdgeInsets.all(.0),
              margin: margin ?? EdgeInsets.all(20),
            ),
          ),
          Text(
            'Sample Text'.toUpperCase(),
            style: TextStyle(fontSize: 50.h),
          ),
        ],
      ),
    );
  }
}
