import 'dart:collection';

import 'package:redux_epic_api_navigation/dto/photo_dto.dart';
import 'package:redux_epic_api_navigation/dto/weather_dto/weather_dto.dart';
import 'package:redux_epic_api_navigation/store/shared/photo_state/photos_actions.dart';
import 'package:redux_epic_api_navigation/store/shared/reducer.dart';
import 'package:redux_epic_api_navigation/store/shared/weather_state/weather_actions.dart';


class WeatherState {

  WeatherDto weather;


  WeatherState({
   this.weather
  });

  factory WeatherState.initial() {
    return WeatherState(
        weather: WeatherDto(),
    );
  }

  WeatherState copyWith({
    WeatherDto recipes,
  }) {
    return WeatherState(
      weather : recipes ?? this.weather,
    );
  }

  WeatherState reducer(dynamic action) {
    return Reducer<WeatherState>(
      actions: HashMap.from({
        SetWeatherToStateAction: (dynamic action) => _setWeather(action as SetWeatherToStateAction),
      }),
    ).updateState(action, this);
  }

  WeatherState _setWeather(SetWeatherToStateAction action) {
    return copyWith(
      recipes: action.weather,
    );
  }
}