import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:redux_epic_api_navigation/dto/weather_main_dto/weather_main_dto.dart';

part 'weather_detail_dto.freezed.dart';

part 'weather_detail_dto.g.dart';

@freezed
abstract class WeatherDetailDto with _$WeatherDetailDto {
  @JsonSerializable(
    fieldRename: FieldRename.snake,
    checked: true,
    explicitToJson: true,
  )
  const factory WeatherDetailDto({
    @JsonKey(name: 'dt') num dt,
    @JsonKey(name: 'main') WeatherMainDto main,


  }) = _WeatherDetailDto;

  factory WeatherDetailDto.fromJson(Map<String, dynamic> json) => _$WeatherDetailDtoFromJson(json);
}