
import 'package:redux/redux.dart';
import 'package:redux_epic_api_navigation/route_helper/routes.dart';
import 'package:redux_epic_api_navigation/services/route_service.dart';
import 'package:redux_epic_api_navigation/store/application/app_state.dart';

class RouteSelectors {

  static void Function() gotoPhotoPage(Store<AppState> store) {
    return () => store.dispatch(RouteService.instance.push(Routes.photoPage));
  }
  static void Function() gotoWeatherPage(Store<AppState> store) {
    return () => store.dispatch(RouteService.instance.push(Routes.weatherPage));
  }
}


