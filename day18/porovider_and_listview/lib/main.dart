import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import './providers/counter_provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (ctx) => CounterProvider(),
      child: MaterialApp(
        title: 'Flutter Demo',
        home: MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    CounterProvider counterProvider = Provider.of<CounterProvider>(context);
    return Scaffold(
      appBar: AppBar(
      ),
      body: Center(
        child: ListView.builder(
          itemCount: counterProvider.listNum.length > 0
              ? counterProvider.listNum.length
              : 0,
          itemBuilder: (context, index) {
            return Container(
              height: MediaQuery.of(context).size.height * 0.1,
              margin: EdgeInsets.all(10),
              alignment: Alignment.center,
              padding: EdgeInsets.all(20),
              color: Colors.blue,
              child: Text('${counterProvider.listNum[index].toString()} - Container'),
            );
          },
        ),
      ),
      bottomNavigationBar: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          FloatingActionButton(
            onPressed: () {
              counterProvider.incrementCounter();
            },
            child: Icon(Icons.add),
          ),
          FloatingActionButton(
            onPressed: () {
              counterProvider.sortList();
            },
            child: Icon(Icons.refresh),
          ),
          FloatingActionButton(
            onPressed: () {
              counterProvider.removeRandom();
            },
            child: Icon(Icons.remove),
          ),
        ],
      ),
    );
  }
}
