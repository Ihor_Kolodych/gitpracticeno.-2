import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  void _gotoDetailsPage(BuildContext context) {
    Navigator.of(context).push(MaterialPageRoute<void>(
      builder: (BuildContext context) => Material(
        color: Colors.black,
        child: Container(
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Hero(
                  tag: 'dash',
                  child: Container(
                    width: 200,
                    height: 200,
                    //color: Colors.blue,
                    child: ColorFiltered(
                      colorFilter: ColorFilter.mode(Colors.grey, BlendMode.modulate),
                      child: Image.network('https://encrypted-tbn0.gstatic'
                          '.com/images?q=tbn:ANd9GcQ6ZFgy5BAvjhqGr6eWhHulrSs-geyMW8BLk8wURrbUPMSZ1DPZjGhbl0C4wUQg7GnZifc&usqp=CAU'),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    ));
  }

  @override
  Widget build(BuildContext context) {
    final PageController controller = PageController(initialPage: 0);
    return PageView(
      scrollDirection: Axis.horizontal,
      controller: controller,
      children: [
        Scaffold(
          appBar: AppBar(
            title: Text(widget.title),
          ),
          body: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  color: Colors.blue,
                  alignment: Alignment.center,
                  width: double.infinity,
                  height: 100.0,
                  child: AspectRatio(
                    aspectRatio: 4 / 3,
                    child: Container(
                      child: Center(
                        child: Text('4:3'),
                      ),
                      color: Colors.red,
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  color: Colors.blue,
                  alignment: Alignment.center,
                  width: double.infinity,
                  height: 100.0,
                  child: AspectRatio(
                    aspectRatio: 16 / 9,
                    child: Container(
                      child: Center(child: Text('16:9')),
                      color: Colors.red,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        Material(
          child: Stack(
            alignment: Alignment.bottomCenter,
            children: [
              Container(
                height: 100,
                width: 100,
                margin: EdgeInsets.all(20.0),
                child: Hero(
                  tag: 'dash',
                  child: GestureDetector(
                    onTap: () => _gotoDetailsPage(context),
                    child: Image.network('https://encrypted-tbn0.gstatic'
                        '.com/images?q=tbn:ANd9GcQ6ZFgy5BAvjhqGr6eWhHulrSs-geyMW8BLk8wURrbUPMSZ1DPZjGhbl0C4wUQg7GnZifc&usqp=CAU'),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
