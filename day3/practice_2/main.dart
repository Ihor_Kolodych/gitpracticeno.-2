import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  void _decrementCounter() {
    setState(() {
      _counter--;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          SizedBox(),
          Container(
            child: Text(
              '$_counter',
              style: TextStyle(fontSize: 40),
            ),
          ),
          Container(
            margin: EdgeInsets.all(25),
            child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
              FloatingActionButton(
                onPressed: _decrementCounter,
                child: Icon(Icons.remove),
              ),
              FloatingActionButton(
                onPressed: _incrementCounter,
                child: Icon(Icons.add),
              ),
            ]),
          ),
        ],
      ),
    );
  }
}

