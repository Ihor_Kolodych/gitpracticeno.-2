import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:redux/redux.dart';
import 'package:redux_navigation/routs/route_selectors.dart';
import 'package:redux_navigation/routs/routs.dart';
import 'package:redux_navigation/store/shared/settings_page_state/settings_page_selectors.dart';


import 'application/app_state.dart';

class SettingsPageViewModel {
  num counter;
  num divide;
  num multiple;
  Color color;
  final void Function(Color color) changeColorBackground;
  final void Function(String route) gotoPage;
  final void Function() popPage;
  final void Function() incrementCounter;
  final void Function() decrementCounter;

  SettingsPageViewModel({
    @required this.counter,
    @required this.color,
    @required this.changeColorBackground,
    @required this.gotoPage,
    @required this.popPage,
    @required this.incrementCounter,
    @required this.decrementCounter,
    @required this.divide,
    @required this.multiple,
  });

  static SettingsPageViewModel fromStore(Store<AppState> store) {
    return SettingsPageViewModel(
      counter: SettingsPageSelectors.getCounterValue(store),
      divide: SettingsPageSelectors.getDivideValueTwo(store),
      multiple: SettingsPageSelectors.getMultipleValueTen(store),
      incrementCounter: SettingsPageSelectors.getIncrementCounterFunction(store),
      decrementCounter: SettingsPageSelectors.getDecrementCounterFunction(store),
      changeColorBackground: SettingsPageSelectors.getChangeColorBackground(store),
      color: SettingsPageSelectors.getColorBackground(store),
      gotoPage: RouteSelectors.gotoPage(store),
      popPage: RouteSelectors.popPage(store),
    );
  }
}
