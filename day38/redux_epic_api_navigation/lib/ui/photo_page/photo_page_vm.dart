import 'package:redux/redux.dart';
import 'package:redux_epic_api_navigation/dto/photo_dto.dart';
import 'package:redux_epic_api_navigation/store/application/app_state.dart';
import 'package:redux_epic_api_navigation/store/shared/photo_state/photos_selector.dart';
import 'package:redux_epic_api_navigation/store/shared/route_selectors.dart';

class PhotoPageVM {

  final void Function() getRecipes;
  final List<PhotoDto> recipesList;
  final void Function() goToWeather;


  PhotoPageVM({
    this.getRecipes,
    this.recipesList,
    this.goToWeather,
  });

  static PhotoPageVM fromStore(Store<AppState> store) {
    return PhotoPageVM(
      getRecipes: PhotosSelectors.getPhotoFunction(store),
      recipesList: PhotosSelectors.photosList(store),
      goToWeather: RouteSelectors.gotoWeatherPage(store),
    );
  }
}