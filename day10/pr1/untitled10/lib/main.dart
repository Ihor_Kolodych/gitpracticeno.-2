
import 'package:flutter/material.dart';
import 'package:untitled10/widgets/custom_inkwell.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with TickerProviderStateMixin {
  AnimationController _animationController;
  AnimationController _colorAnimationController;
  AnimationController _opacityAnimationController;

  Animation<double> _size;
  Animation<Color> _color;
  Animation<double> _opacity;
  bool hide = false;

  @override
  void initState() {
    _animationController = AnimationController(vsync: this, duration: Duration(milliseconds: 400), reverseDuration: Duration(seconds: 2));
    _colorAnimationController = AnimationController(vsync: this, duration: Duration(milliseconds: 400), reverseDuration: Duration(milliseconds: 400));
    _opacityAnimationController = AnimationController(vsync: this, duration: Duration(seconds: 2), reverseDuration: Duration(seconds: 2));
    _size = Tween<double>(begin: 0, end: 100).animate(_animationController);
    _color = ColorTween(begin: Colors.red, end: Colors.lightGreenAccent).animate(_colorAnimationController);
    _opacity = Tween<double>(begin: 0.0, end: 1.0).animate(_opacityAnimationController);

    _opacityAnimationController.addListener(() {
      setState(() {});
    });
    _colorAnimationController.addListener(() {
      setState(() {});
    });
    super.initState();
  }

  void ifClick() {
    if (_animationController.isCompleted && _colorAnimationController.isCompleted && _opacityAnimationController.isCompleted) {
      _opacityAnimationController.reverse();
      _colorAnimationController.reverse();
      _animationController.reverse();
    } else {
      _opacityAnimationController.forward();
      _colorAnimationController.forward();
      _animationController.forward();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                child: AnimatedBuilder(
                  animation: _animationController,
                  builder: (BuildContext context, Widget _) {
                    return Container(
                      color: _color.value,
                      width: double.infinity,
                      height: _size.value,
                      child: _animationController.isCompleted
                          ? Center(
                              child: Opacity(
                                  opacity: _opacity.value,
                                  child: Text(
                                    'Hello World!',
                                    style: TextStyle(fontSize: 30),
                                  )))
                          : Text('')
                    );
                  },
                ),
              ),
              CustomButton(
                onTap: () {
                  ifClick();
                },
                child: AnimatedBuilder(
                  animation: _animationController,
                  builder: (BuildContext context, Widget _) {
                    return Container(
                      color: _color.value,
                      width: 100,
                      height: 100,
                    );
                  },
                ),
              ),
            ],
          ),
        ));
  }
}
