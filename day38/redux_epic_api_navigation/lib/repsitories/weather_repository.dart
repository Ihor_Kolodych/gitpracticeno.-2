import 'dart:convert';

import 'package:redux_epic_api_navigation/dto/weather_dto/weather_dto.dart';
import 'package:redux_epic_api_navigation/services/weather_service.dart';

class WeatherRepository {
  WeatherRepository._privateConstructor();

  static final WeatherRepository instance =
  WeatherRepository._privateConstructor();

  Future<WeatherDto> getWeather() async {
    var responseData = await WeatherService.instance.getWeather();
    print('RESPONCE DATA ${responseData.response.runtimeType}');
    //  print('FROM JSON REWSPONCE DATA ${jsonDecode(responseData.response)}');
    WeatherDto weatherDto = WeatherDto.fromJson(responseData.response);
    print(weatherDto);
    return weatherDto;
    //print(WeatherDto.fromJson(responseData.response['city']));
    //return WeatherDto.fromJson(responseData.response['city']);
    //return WeatherDto.fromJson(json.decode(responseData.response.body)['city']);
   // return weather;
  }
}
