import 'dart:collection';

import 'package:redux_epic_api_navigation/dto/photo_dto.dart';
import 'package:redux_epic_api_navigation/store/shared/photo_state/photos_actions.dart';
import 'package:redux_epic_api_navigation/store/shared/reducer.dart';


class PhotoState {

  List<PhotoDto> photos;


  PhotoState({
   this.photos
  });

  factory PhotoState.initial() {
    return PhotoState(
    photos: []
    );
  }

  PhotoState copyWith({
    List<PhotoDto> recipes,
  }) {
    return PhotoState(
        photos : recipes ?? this.photos,
    );
  }

  PhotoState reducer(dynamic action) {
    return Reducer<PhotoState>(
      actions: HashMap.from({
        SetPhotosToStateAction: (dynamic action) => _setRecipes(action as SetPhotosToStateAction),
      }),
    ).updateState(action, this);
  }

  PhotoState _setRecipes(SetPhotosToStateAction action) {
    return copyWith(
      recipes: action.photos
    );
  }
}