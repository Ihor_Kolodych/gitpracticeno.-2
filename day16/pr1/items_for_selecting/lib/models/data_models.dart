import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:items_for_selecting/models/date_storage_model.dart';

import 'custom_container.dart';

class DataModel extends StatefulWidget {
  @override
  _DataModelState createState() => _DataModelState();
}

class _DataModelState extends State<DataModel> {
  DateTime _dateTime;
  TimeOfDay _timeOfDay;
  double _currentSliderValue = 0;
  String _valueDropdownButton;
  //final List<ListItem> items;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        CustomContainer(
          color: Colors.lightGreen,
          child: InkWell(
            onTap: () {
              showDatePicker(
                context: context,
                initialDate: DateTime.now(),
                firstDate: DateTime(2020),
                lastDate: DateTime(2022),
              ).then((date) {
                setState(() {
                  _dateTime = date;
                });
              });
            },
            child: Center(
              child: CustomContainer(
                color: Colors.lightGreenAccent,
                child: Text(
                  _dateTime == null ? 'Nothing has been picked yet' : DateFormat.yMMMd().format(_dateTime).toString(),
                ),
              ),
            ),
          ),
        ),
        CustomContainer(
          color: Colors.green,
          child: InkWell(
            onTap: () {
              String houres = DateFormat.H().format(
                DateTime.now(),
              );
              String minuts = DateFormat.m().format(
                DateTime.now(),
              );
              showTimePicker(
                context: context,
                initialTime: TimeOfDay(hour: int.parse(houres), minute: int.parse(minuts)),
              ).then((date) {
                setState(() {
                  _timeOfDay = date;
                });
              });
            },
            child: Center(
              child: CustomContainer(
                color: Colors.lightGreenAccent,
                child: Text(
                  _timeOfDay == null ? 'Nothing has been picked yet' : _timeOfDay.format(context),
                ),
              ),
            ),
          ),
        ),
        CustomContainer(
          color: Colors.greenAccent,
          child: Slider(
              value: _currentSliderValue,
              min: 0,
              max: 1,
              //divisions: 1,
              label: _currentSliderValue.round().toString(),
              onChanged: (double value) {
                setState(() {
                  _currentSliderValue = value;
                });
              }),
        ),
        CustomContainer(
          color: Colors.lightGreen,
          child: DropdownButton(
              value: _valueDropdownButton,
              items: [
                DropdownMenuItem(
                  child: Text("Male"),
                  value: "Male",
                ),
                DropdownMenuItem(
                  child: Text("Female"),
                  value: "Female",
                ),
              ],
              onChanged: (value) {
                setState(() {
                  _valueDropdownButton = value;
                });
              }),
        ),
        CustomContainer(
          color: Colors.brown,
          child: InkWell(
            onTap: () {
              setState(() {
                print(DateStore(DateFormat.yMMMd().format(_dateTime), _timeOfDay.format(context), _currentSliderValue,
                    _valueDropdownButton)
                    .toString(),);
              });
            },
          ),
        )
      ],
    );
  }
}
