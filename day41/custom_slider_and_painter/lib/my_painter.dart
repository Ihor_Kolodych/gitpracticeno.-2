import 'package:flutter/material.dart';

class CustomSliderPainter extends CustomPainter{
  //double totalWidth = 200.0;
  double valueInitial;

  CustomSliderPainter({this.valueInitial});
  @override

    void paint(Canvas canvas, Size size) {
      var paint = Paint()
        ..color = Colors.deepOrange
        ..strokeWidth = 5
        ..strokeCap = StrokeCap.round;

      Offset startingPoint = Offset(3, size.height / 2);
      Offset endingPoint = Offset(size.width - 3, size.height / 2);

      canvas.drawLine(startingPoint, endingPoint, paint);
      var circle= Paint()
        ..color = Colors.blue;

      Offset starting = Offset(valueInitial, size.height / 2);
      canvas.drawCircle(starting, 8, circle);
    }


    @override
    bool shouldRepaint(CustomSliderPainter oldDelegate) {
      valueInitial != oldDelegate.valueInitial;
      return true;
    }

}