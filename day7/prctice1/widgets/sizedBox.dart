import 'package:flutter/material.dart';
import 'package:untitled3/widgets/selectableText.dart';

class SizedBoxWidget extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 10, bottom: 10),
      child: SizedBox(
        width: double.infinity,
        height: 100,
        child: ElevatedButton(
          onPressed: () {},
          child: SelectableTextWidget(
          ),
        ),
      ),
    );
  }
}
