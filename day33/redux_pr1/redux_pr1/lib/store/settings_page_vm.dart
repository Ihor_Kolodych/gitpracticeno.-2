import 'package:flutter/foundation.dart';
import 'package:redux/redux.dart';
import 'package:redux_pr1/store/application/app_state.dart';
import 'package:redux_pr1/store/shared/settings_page_state/settings_page_selectors.dart';

class SettingsPageViewModel {
  num counter;
  final void Function() incrementCounter;
  final void Function() resetCounter;
  final void Function() changeColor;
  final bool getBoolean;


  SettingsPageViewModel({
    @required this.counter,
    @required this.incrementCounter,
    @required this.resetCounter,
    @required this.changeColor,
    @required this.getBoolean,
  });

  static SettingsPageViewModel fromStore(Store<AppState> store) {
    return SettingsPageViewModel(
        counter: SettingsPageSelectors.getCounterValue(store),
        incrementCounter: SettingsPageSelectors.getIncrementCounterFunction(store),
        resetCounter: SettingsPageSelectors.resetCounter(store),
        changeColor: SettingsPageSelectors.changeColor(store),
        getBoolean: SettingsPageSelectors.getBoolean(store)
    );
  }
}
