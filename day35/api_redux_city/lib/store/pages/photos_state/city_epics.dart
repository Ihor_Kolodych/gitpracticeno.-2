
import 'package:api_redux_city/dto/city_dto.dart';
import 'package:api_redux_city/repsitories/city_repository.dart';
import 'package:api_redux_city/store/app/app_state.dart';
import 'package:api_redux_city/store/pages/photos_state/city_action.dart';
import 'package:redux_epics/redux_epics.dart';
import 'package:rxdart/rxdart.dart';

import '../../shared/empty_action.dart';

class CityEpics {
  static final indexEpic = combineEpics<AppState>([
    getCitysEpic,
    setCitysEpic,
  ]);

  static Stream<dynamic> getCitysEpic(Stream<dynamic> actions, EpicStore<AppState> store) {
    return actions.whereType<GetCityAction>().switchMap(
      (action) {
         // List<ImageDto> images = await UnsplashImageRepository.instance.getImages();
         // if(images!= null && images.isNotEmpty){
         //   Stream.value(SaveImagesAction(images: images));
         // }
        return Stream.fromFuture(
          CityRepository.instance.getCitys().then(
            (List<CityDto> citys) {
              if (citys == null) {
                return EmptyAction();
              }
              return SaveCityAction(
                citys: citys,
              );
            },
          ),
        );
      },
    );
  }

  static Stream<dynamic> setCitysEpic(Stream<dynamic> actions, EpicStore<AppState> store) {
    return actions.whereType<SaveCityAction>().switchMap(
      (action) {
        return Stream.fromIterable(
          [
            EmptyAction(),
          ],
        );
      },
    );
  }

}
