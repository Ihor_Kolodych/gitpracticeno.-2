
import 'package:flutter/material.dart';

class People {
  String name;
  Icon icon;
  String city;
  People({this.name, this.icon, this.city});
}