import 'package:flutter/material.dart';
import './models/people.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final List<People> people = [
    People(name: 'Ihor', icon: Icon(Icons.add), city: 'Odessa'),
    People(name: 'Ihor2', icon: Icon(Icons.remove), city: 'Odessa'),
    People(name: 'Ihor3', icon: Icon(Icons.add_circle_outline), city: 'Odessa'),
    People(name: 'Ihor4', icon: Icon(Icons.remove_circle), city: 'Odessa'),
    People(name: 'Ihor', icon: Icon(Icons.add), city: 'Odessa'),
    People(name: 'Ihor2', icon: Icon(Icons.remove), city: 'Odessa'),
    People(name: 'Ihor3', icon: Icon(Icons.add_circle_outline), city: 'Odessa'),
    People(name: 'Ihor4', icon: Icon(Icons.remove_circle), city: 'Odessa'),
    People(name: 'Ihor4', icon: Icon(Icons.remove_circle), city: 'Odessa'),
    People(name: 'Ihor4', icon: Icon(Icons.remove_circle), city: 'Odessa'),
    People(name: 'Ihor4', icon: Icon(Icons.remove_circle), city: 'Odessa'),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            leading: IconButton(
                icon: Icon(Icons.add),
                onPressed: (){},
              ),
            centerTitle: true,
            expandedHeight: 100,
            title: Text('SliverAppBar'),
          ),
          SliverList(
            delegate: SliverChildBuilderDelegate(
              (BuildContext context, int index) {
                return ListTile(
                  leading: people[index].icon,
                  title: Text(
                    people[index].name,
                  ),
                  subtitle: Text(
                    people[index].city,
                  ),
                );
              },
              childCount: people.length,
            ),
          ),
        ],
      ),
    );
  }
}
