

import 'package:redux_epic_api_navigation/dto/photo_dto.dart';
import 'package:redux_epic_api_navigation/store/shared/base_action.dart';

class GetPhotosAction extends BaseAction {

}

class SetPhotosToStateAction extends BaseAction {
  final List<PhotoDto> photos;

  SetPhotosToStateAction(this.photos);
}