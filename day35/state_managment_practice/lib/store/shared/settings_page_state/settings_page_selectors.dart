import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:state_managment_practice/store/application/app_state.dart';
import 'package:state_managment_practice/store/shared/settings_page_state/settings_page_action.dart';


class SettingsPageSelectors {

  static void Function(Color) getChangeColorBackground(Store<AppState> store) {
    return (Color color) => store.dispatch(ChangeColorBackgroundAction(color: color));
  }

  static Color getColorBackground(Store<AppState> store) {
    return store.state.settingsPageState.color;
  }

}