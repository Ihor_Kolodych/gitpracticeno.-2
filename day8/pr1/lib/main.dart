import 'package:flutter/material.dart';
import 'package:flutter_complete_guide/page_ex.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: Size(1080, 1000),
      builder: () => MaterialApp(
        theme: ThemeData(
          textTheme: TextTheme(
            button: TextStyle(
                fontSize: 200.sp
            ),
          ),
          primaryColor: Colors.deepOrangeAccent,
        ),
        home: MyHomePage(),
      ),
    );
  }
}

