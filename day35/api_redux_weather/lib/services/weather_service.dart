
import 'dart:convert';

import 'package:api_redux_weather/services/network_service.dart';

import '../base_network_service.dart';
import 'package:http/http.dart' as http;
class WeatherService {
  WeatherService._privateConstructor();

  static final WeatherService instance =
      WeatherService._privateConstructor();
  static String _url = 'http://api.openweathermap.org/data/2.5/weather?q=Kherson&appid=86f68728cbe4671b38728a2bb6226b74';

  Future<Map<String, dynamic>> getImages() async {
    var responseData =
        await http.get(_url);
    print(jsonDecode(responseData.body));
    return jsonDecode(responseData.body);
  }
}
