// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'weather_city_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_WeatherCityDto _$_$_WeatherCityDtoFromJson(Map<String, dynamic> json) {
  return $checkedNew(r'_$_WeatherCityDto', json, () {
    final val = _$_WeatherCityDto(
      name: $checkedConvert(json, 'name', (v) => v as String),
    );
    return val;
  });
}

Map<String, dynamic> _$_$_WeatherCityDtoToJson(_$_WeatherCityDto instance) =>
    <String, dynamic>{
      'name': instance.name,
    };
