import 'package:redux/redux.dart';
import 'package:redux_epic_api_navigation/dto/photo_dto.dart';
import 'package:redux_epic_api_navigation/dto/weather_dto/weather_dto.dart';
import 'package:redux_epic_api_navigation/store/application/app_state.dart';
import 'package:redux_epic_api_navigation/store/shared/photo_state/photos_selector.dart';
import 'package:redux_epic_api_navigation/store/shared/route_selectors.dart';
import 'package:redux_epic_api_navigation/store/shared/weather_state/weather_selector.dart';

class WeatherPageVM {

  final void Function() getWeather;
  final WeatherDto weatherList;
  final void Function() goToPhoto;


  WeatherPageVM({
    this.getWeather,
    this.weatherList,
    this.goToPhoto,
  });

  static WeatherPageVM fromStore(Store<AppState> store) {
    return WeatherPageVM(
      getWeather: WeatherSelectors.getWeatherFunction(store),
      weatherList: WeatherSelectors.weatherList(store),
      goToPhoto: RouteSelectors.gotoPhotoPage(store),
    );
  }
}