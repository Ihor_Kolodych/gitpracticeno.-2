import 'package:redux/redux.dart';
import 'package:redux_epic_api_navigation/dto/weather_dto/weather_dto.dart';
import 'package:redux_epic_api_navigation/store/application/app_state.dart';
import 'package:redux_epic_api_navigation/store/shared/weather_state/weather_actions.dart';

class WeatherSelectors{
  static void Function() getWeatherFunction(Store<AppState> store){
    return () => store.dispatch(GetWeatherAction());
  }
  static WeatherDto weatherList(Store<AppState> store){
    print('www ${store.state.weatherState.weather}');
    return store.state.weatherState.weather;
  }
}
