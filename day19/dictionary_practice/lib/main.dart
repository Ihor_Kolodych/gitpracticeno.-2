
import 'package:dictionary_practice/providers/language_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'application/application.dart';

void main() {
  runApp(
    ChangeNotifierProvider(
      builder: (BuildContext ctx) {
        return LanguageProvider();
      },
      child: Application(),
    ),
  );
}
