
import 'dart:convert';

import 'package:api_redux_city/services/network_service.dart';

import '../base_network_service.dart';
class CityService {
  CityService._privateConstructor();

  static final CityService instance =
      CityService._privateConstructor();
  static String _url = 'https://api.openbrewerydb.org/breweries';

  Future<BaseNetworkService> getCitys() async {
    var responseData =
    await NetworkService.instance.request(HttpType.get, _url, null, null);
    print(responseData.response);
    return responseData;
  }
}
