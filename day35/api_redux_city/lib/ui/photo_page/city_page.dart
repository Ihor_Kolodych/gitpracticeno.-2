import 'dart:math';

import 'package:api_redux_city/store/app/app_state.dart';
import 'package:api_redux_city/ui/photo_page/city_page_view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

class PhotoPage extends StatefulWidget {
  @override
  _PhotoPageState createState() => _PhotoPageState();
}

class _PhotoPageState extends State<PhotoPage> {
  List colors = [
    Colors.blue.withOpacity(0.2),
    Colors.blue.withOpacity(0.4),
    Colors.blue.withOpacity(0.6),
    Colors.blue.withOpacity(0.8),
    Colors.blue.withOpacity(1.0),
  ];
  Random random = new Random();

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, CityPageViewModel>(
      converter: CityPageViewModel.fromStore,
      onInitialBuild: (CityPageViewModel viewModel) => viewModel.getCity(),
      builder: (BuildContext context, CityPageViewModel viewModel) {
        return Material(
          child: Container(
            child: ListView.builder(
              itemCount: viewModel.weather.length,
              itemBuilder: (BuildContext ctx, int index) => Center(
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(50.0),
                  child: Container(
                    color: colors[random.nextInt(5)],
                    height: 100.0,
                    margin: const EdgeInsets.all(10.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'Id: ${viewModel.weather[index].id}' ?? '',
                          textAlign: TextAlign.center,
                          maxLines: 2,
                        ),
                        Text(
                          'State: ${viewModel.weather[index].state}' ?? '',
                          textAlign: TextAlign.center,
                          maxLines: 2,
                        ),
                        Text(
                          'Country: ${viewModel.weather[index].country}' ?? '',
                          textAlign: TextAlign.center,
                          maxLines: 2,
                        ),
                        Text(
                          'City: ${viewModel.weather[index].city}' ?? '',
                          textAlign: TextAlign.center,
                          maxLines: 2,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
